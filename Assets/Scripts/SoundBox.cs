﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SoundBox : MonoBehaviour
{
    [SerializeField] public AudioClip ClickingSFX;
    [SerializeField] public AudioClip Music;

    private void Start()
    {
        SoundManagerSingleton.Instance.Unmute();
        if (SoundManagerSingleton.Instance.musicSource.clip != Music)
        {
            SoundManagerSingleton.Instance.PlayMusic(Music);
        }
    }

    public void Clicking()
    {
        SoundManagerSingleton.Instance.PlaySFX(ClickingSFX);
    }
    
    public void MuteOrUnmute()
    {
        SoundManagerSingleton.Instance.MuteOrUnmute();
    }

    public void ChangeMasterVol(float value)
    {
        SoundManagerSingleton.Instance.ChangeMasterVol(value);
    }
    
    public void ChangeMusicVol(float value)
    {
        SoundManagerSingleton.Instance.ChangeMusicVol(value);
    }
    
    public void ChangeSFXVol(float value)
    {
        SoundManagerSingleton.Instance.ChangeSFXVol(value);
    }
}
