﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraClamp : MonoBehaviour
{
    [SerializeField] private Vector2 clampValue =Vector2.zero;

    [SerializeField] private Vector2 offset;
    private Camera _camera;
    
    private void Start()
    {
        _camera = GetComponent<Camera>();
        _camera.orthographicSize = 9.5f;
    }

    void Update()
    {
        if(_camera.orthographicSize > 9.5f)
            _camera.orthographicSize = 9.5f;
        
        transform.position = new Vector3(Mathf.Clamp(transform.position.x + offset.x, clampValue.x, clampValue.x),
            Mathf.Clamp(transform.position.y+ offset.y, clampValue.y, clampValue.y),
            -15f);
    }
}
