﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ActiveCameraClamp : MonoBehaviour
{
    public CameraClamp cameraClamp;
    // Start is called before the first frame update
    void Start()
    {
        cameraClamp.enabled = true;
    }
}
