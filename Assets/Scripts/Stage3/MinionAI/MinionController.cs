﻿using System;
using Photon.Pun;
using UnityEngine;
using Random = UnityEngine.Random;
using NungTalungAR.Stage3.Player;

namespace NungTalungAR.Stage3.MinionAI
{
    [RequireComponent(typeof(PhotonTransformView))]
    [RequireComponent(typeof(PhotonRigidbody2DView))]
    public class MinionController : MonoBehaviourPun, IPunInstantiateMagicCallback
    {
        private Vector2 randomPickedTraget;
        private int REMAIN_HP = 3;

        public void OnPhotonInstantiate(PhotonMessageInfo info)
        {
            if (!PhotonNetwork.IsMasterClient)
                return;
        }

        void Update()
        {
            if (!PhotonNetwork.IsMasterClient)
                return;
            PlayerController[] temp = GameObject.FindObjectsOfType<PlayerController>();
            randomPickedTraget = new Vector2(temp[Random.Range(0, temp.Length)].transform.position.x,
                temp[Random.Range(0, temp.Length)].transform.position.y);
            this.transform.position = Vector2.MoveTowards(this.transform.position,
                randomPickedTraget + new Vector2(0, 2), 7 * Time.deltaTime);
            this.transform.Rotate(0, 0, 10);
        }

        public void ChangeHealth(int amount)
        {
            REMAIN_HP += amount;
            if (REMAIN_HP <= 0)
            {
                Destroy(this.gameObject);
            }
        }

        private void OnCollisionEnter2D(Collision2D other)
        {
            if (other.gameObject.CompareTag("Player"))
            {
                other.gameObject.GetComponent<PlayerController>().ChangeHealth(-1);
                Destroy(this.gameObject);
            }
        }

        private void OnDestroy()
        {
            if (!photonView.IsMine)
                return;

            PhotonNetwork.Destroy(this.gameObject);
        }
    }
}
