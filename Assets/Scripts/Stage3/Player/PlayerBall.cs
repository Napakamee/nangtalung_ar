﻿ using System;
using Photon.Pun;
using UnityEngine;

 namespace NungTalungAR.Stage3.Player
{
    public class PlayerBall : MonoBehaviourPun, IPunInstantiateMagicCallback
    {
        private bool isHit = false;
        int OwnerViewID = -1;
        [SerializeField] private AudioClip LaunchingSFX;
        [SerializeField] private AudioClip HitSFX;
        [SerializeField] private AudioSource BallSFX;
        
        
        public void OnPhotonInstantiate(PhotonMessageInfo info)
        {
            // e.g. store this gameobject as this player's charater in Player.TagObject
            info.Sender.TagObject = this.gameObject;
            OwnerViewID = info.photonView.ViewID;

            BallSFX.clip = LaunchingSFX;
            BallSFX.Play();
            
            if (!photonView.IsMine)
                return;

            // Destroy the bullet after 10 seconds
            Destroy(this.gameObject, 3.0f);
        }

        private void OnCollisionEnter2D(Collision2D other)
        {
            BallSFX.clip = HitSFX;
            BallSFX.Play();
            
            if(!photonView.IsMine) return;

            if (other.gameObject.CompareTag("Enemy"))
            {
                other.gameObject.GetComponent<YakAI.YakAI>().ChangeHealth(-1);
                Destroy(this.gameObject);
            }
            
        }
        
        private void OnDestroy()
        {
            if (!photonView.IsMine) return;
        
            PhotonNetwork.Destroy(this.gameObject);
        }
    }
}