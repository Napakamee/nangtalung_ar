﻿using System.IO;
using Photon.Pun;
using SimpleJSON;
using TMPro;
using UnityEngine;
using UnityEngine.UI;
using Hashtable = ExitGames.Client.Photon.Hashtable;

namespace NungTalungAR.Stage3.Player
{
    public class PlayerSetting
    {
        public const string PLAYER_COLOR = "PlayerColor";

        public static Color GetColor(int colorChoice)
        {
            switch (colorChoice)
            {
                case 0: return Color.red;
                case 1: return new Color(1, 1, 1, 0.5f);
                case 2: return Color.white;
                case 3: return Color.green;
                case 4: return Color.yellow;
                case 5: return Color.blue;
                case 6: return Color.magenta;
            }

            return Color.white;
        }
    }

    [RequireComponent(typeof(PhotonTransformView))]
    public class PlayerController : MonoBehaviourPunCallbacks, IPunObservable
    {
        public int maxHealth = 3;
        private int currentHealth;
        [SerializeField] private AudioClip HitHurt;
        [SerializeField] private AudioClip HitDead;
        [SerializeField] private AudioSource PlayerSFX;
        [SerializeField] private GameObject[] Hearts;
        public float walkSpeed = 2.5f;
        public float maxInvincibleDurationAfterHit = 2;
        private float currentInvincibleTime;

        public Vector3 centerOffset;
        public Transform GroundCheckTransform;
        public float groundCheckRadius = 0.2f;

        private Transform tragetTransform;
        public LayerMask groundMask;

        private float inputMovement;
        private Animator animator;
        private Rigidbody2D rb;
        private bool isGrounded;
        private Camera mainCamera;
        private bool isDeath = false;
        private bool invincible;

        [Header("Player Component")]
        [SerializeField] private Collider2D boxCollider;
        [SerializeField] private GameObject playerPref;
        [SerializeField] private GameObject throwZone;
        [SerializeField] private GameObject imageObj;
        [SerializeField] private string nameString;
        [SerializeField] private TextMeshProUGUI nameText;
        private int characterIndex;
        private string[] nameIndex;
        
        [Header("Mouth")]
        [SerializeField] private Transform mouthIK;
        [SerializeField] private float mouthPosA = 0.0f;
        [SerializeField] private float mouthPosB = -0.25f;
        [SerializeField] private float mouthMoveSpeed = 0.5f;
        [SerializeField] bool keyDown = false;
        
        [Header("Clamp Value")] 
        [SerializeField] private float minClampX = -15.7f;
        [SerializeField] private float maxClampX = 15.7f;

        [SerializeField] private float minClampY = -7f;
        [SerializeField] private float maxClampY = 7f;

        [SerializeField] private float aliveClampX = 15.7f;
        [SerializeField] private float aliveClampY = 7f;
        [SerializeField] private float deathClampX = 5f;
        [SerializeField] private float deathClampY = 3f;
        
        void Start()
        {
            minClampX = -aliveClampX;
            maxClampX = aliveClampX;
            
            minClampY = -aliveClampY;
            maxClampY = aliveClampY;
            
            GetName();
            rb = GetComponent<Rigidbody2D>();
            mainCamera = Camera.main;
            currentHealth = maxHealth;
        }

        private void GetName()
        {
            
            string path = Application.streamingAssetsPath + "/JsonData/PlayerCharacterIndex.json";
            string jsonString = File.ReadAllText((path));
            JSONObject charIndex = (JSONObject) JSON.Parse(jsonString);
            characterIndex = charIndex["CharIndex"];
            nameString = photonView.Owner.NickName;
            
            nameIndex = new string[12]
            {
                CharacterName.NUI, CharacterName.TENG, CharacterName.YODTHONG, CharacterName.SRIKAEW, CharacterName.PHUYAIPOON,
                CharacterName.KWANMUANG, CharacterName.AITHO, CharacterName.AISAMOR, CharacterName.HERMIT,
                CharacterName.GIANT, CharacterName.SPECTATOR, nameString
            };
            SetName();
        }

        private void SetName()
        {
            //Debug.Log(nameIndex[characterIndex]);
            nameText.text = nameString;
        }
        void Update()
        {
            if (!photonView.IsMine)
                return;
            
            if (gameObject.CompareTag("PlayerDeath") && !isDeath)
            {
                isDeath = true;
                Image ghostImage = imageObj.GetComponent<Image>();
                ghostImage.enabled = true;
                Death();
            }
            
            transform.position = new Vector3(
                Mathf.Clamp(transform.position.x, minClampX, maxClampX),
                Mathf.Clamp(transform.position.y, minClampY, maxClampY),
                transform.position.z);
            
            if (Input.GetKeyUp(KeyCode.Space))
            {
                keyDown = false;
            }
            
        }

        private void FixedUpdate()
        {
            // Ground Check
            isGrounded = Physics2D.OverlapCircle(GroundCheckTransform.position, groundCheckRadius, groundMask);

            if (photonView.IsMine)
            {
                MoveMouth();
                
                
                if (currentInvincibleTime > maxInvincibleDurationAfterHit - 0.5f)
                {
                    currentInvincibleTime -= Time.deltaTime;
                    photonView.RPC("RPCRed",RpcTarget.All);
                    /*Hashtable props = new Hashtable
                    {
                        {PlayerSetting.PLAYER_COLOR, 0}
                    };
                    PhotonNetwork.LocalPlayer.SetCustomProperties(props);*/
                    invincible = true;
                }
                else if (currentInvincibleTime > 0)
                {
                    currentInvincibleTime -= Time.deltaTime;
                    photonView.RPC("RPCTrans",RpcTarget.All);
                    /*
                    Hashtable props = new Hashtable
                    {
                        {PlayerSetting.PLAYER_COLOR, 1}
                    };
                    PhotonNetwork.LocalPlayer.SetCustomProperties(props);*/
                    invincible = true;
                }
                else
                {
                    photonView.RPC("RPCWhite",RpcTarget.All);
                    /*
                    Hashtable props = new Hashtable
                    {
                        {PlayerSetting.PLAYER_COLOR, 2}
                    };
                    PhotonNetwork.LocalPlayer.SetCustomProperties(props);*/
                    invincible = false;
                }
            }

            if (!photonView.IsMine)
                return;
        
        }
        private void MoveMouth()
        {
            Vector3 position = mouthIK.transform.localPosition;
            
            if (Input.GetKey(KeyCode.Space) && position.y >= mouthPosB)
            {
                keyDown = true;
                mouthIK.Translate(new Vector3(0, -mouthMoveSpeed * Time.deltaTime,0)); 
            }
            
            if (!keyDown && position.y <= mouthPosA)
            {
                mouthIK.Translate(new Vector3(0, mouthMoveSpeed * Time.deltaTime,0));
            }
            if (!keyDown && position.y >= 0)
            {
                position.y = 0;
            }
            
        }
        
        public void ChangeHealth(int amount)
        {
            if (!invincible)
            {
                if (amount < 0)
                {
                    currentInvincibleTime = maxInvincibleDurationAfterHit;
                    photonView.RPC("soundHitHurt", RpcTarget.All);
                }

                currentHealth += amount;
                Debug.Log(this.name + "'s remaining hp = " + currentHealth);
                if (currentHealth <= 0)
                {
                    if (photonView.IsMine)
                    {
                        PlayerKnockedOut();
                        photonView.RPC("soundHitDead", RpcTarget.All);
                    }
                }
                
                photonView.RPC("HeartSetActive", RpcTarget.All);
            }
        }

        public void PlayerKnockedOut()
        {
            Death();
        }

        public void OnPhotonSerializeView(PhotonStream stream, PhotonMessageInfo info)
        {
            if (stream.IsWriting)
            {
                stream.SendNext(currentHealth);
            }
            else
            {
                currentHealth = (int) stream.ReceiveNext();
            }
        }

        
        public void Death()
        {
            if (photonView != null)
                photonView.RPC("RPCDeath", RpcTarget.All);
            minClampX = -deathClampX;
            maxClampX = deathClampX;
            minClampY = -deathClampY;
            maxClampY = deathClampY;
        }
        
        [PunRPC]
        public void RPCDeath()
        {
            Debug.Log("Death");
            this.tag = "PlayerDeath";
            boxCollider.enabled = false;
            playerPref.SetActive(false);
            throwZone.SetActive(false);
            nameText.enabled = false;
            rb.simulated = false;
            Hashtable props = new Hashtable
            {
                {PunGameSetting.DEATHPLAYER, 1}
            };
            PhotonNetwork.CurrentRoom.SetCustomProperties(props);
        }

        [PunRPC]
        public void RPCRed()
        {
            SpriteRenderer[] sr = GetComponentsInChildren<SpriteRenderer>();
            foreach (var _sr in sr)
            {
                _sr.color = PlayerSetting.GetColor((int)0);
            }
        }

        [PunRPC]
        public void RPCTrans()
        {
            SpriteRenderer[] sr = GetComponentsInChildren<SpriteRenderer>();
            foreach (var _sr in sr)
            {
                _sr.color = PlayerSetting.GetColor((int)1);
            }
        }

        [PunRPC]
        public void RPCWhite()
        {
            SpriteRenderer[] sr = GetComponentsInChildren<SpriteRenderer>();
            foreach (var _sr in sr)
            {
                _sr.color = PlayerSetting.GetColor((int)2);
            }
        }

        [PunRPC]
        public void HeartSetActive()
        {
            switch (currentHealth)
            {
                case 0 : Hearts[0].SetActive(false); Hearts[1].SetActive(false); Hearts[2].SetActive(false); break;
                case 1 : Hearts[0].SetActive(true); Hearts[1].SetActive(false); Hearts[2].SetActive(false); break;
                case 2 : Hearts[0].SetActive(true); Hearts[1].SetActive(true); Hearts[2].SetActive(false); break;
                case 3 : Hearts[0].SetActive(true); Hearts[1].SetActive(true); Hearts[2].SetActive(true); break;
                default: Hearts[0].SetActive(true); Hearts[1].SetActive(true); Hearts[2].SetActive(true); break;
            }
        }

        [PunRPC]
        public void soundHitHurt()
        {
            PlayerSFX.clip = HitHurt;
            PlayerSFX.Play();
        }
        
        [PunRPC]
        public void soundHitDead()
        {
            PlayerSFX.clip = HitDead;
            PlayerSFX.Play();
        }
    }
}
