﻿using Photon.Pun;
using UnityEngine;
using NungTalungAR.Stage3.Player;

namespace NungTalungAR.Stage3.YakAI
{
    [RequireComponent(typeof(PhotonTransformView))]
    [RequireComponent(typeof(PhotonRigidbody2DView))]
    public class FadeOffFireBallController : MonoBehaviourPun, IPunInstantiateMagicCallback
    {
        private float countdown = 1.5f;
        public float velocity;
        private Vector2 randomPickedTraget;
        private bool TragetSet = false;
        [SerializeField] private AudioClip FireBallSFX;
        [SerializeField] private AudioClip HitSFX;
        [SerializeField] private AudioSource FBFX;

        public void OnPhotonInstantiate(PhotonMessageInfo info)
        {
            FBFX.clip = FireBallSFX;
            FBFX.Play();
            if (!PhotonNetwork.IsMasterClient)
                return;

            Destroy(this.gameObject, 15.0f);
        }

        private void FixedUpdate()
        {
            if (!PhotonNetwork.IsMasterClient)
                return;

            if (countdown > 0)
            {
                countdown -= Time.deltaTime;
            }
            else if (!TragetSet)
            {
                PlayerController[] temp = GameObject.FindObjectsOfType<PlayerController>();
                randomPickedTraget = new Vector2(temp[Random.Range(0, temp.Length)].transform.position.x,
                    temp[Random.Range(0, temp.Length)].transform.position.y);
                TragetSet = true;
            }
            else
            {
                this.transform.position = Vector3.MoveTowards(this.transform.position, randomPickedTraget + Vector2.down*3f,
                    velocity * 2 * Time.deltaTime);
            }
        }

        private void OnCollisionEnter2D(Collision2D other)
        {
            FBFX.clip = HitSFX;
            FBFX.Play();
            
            if (other.gameObject.CompareTag("Player"))
            {
                other.gameObject.GetComponent<PlayerController>().ChangeHealth(-1);
                Destroy(this);
            }
            else if (other.gameObject.CompareTag("Ground"))
            {
                Destroy(this);
            }
        }

        private void OnDestroy()
        {
            if (!photonView.IsMine)
                return;

            PhotonNetwork.Destroy(this.gameObject);
        }
    }
}
