﻿using System.Collections;
using UnityEngine;
using NungTalungAR.Stage3.Player;
using SimpleJSON;

namespace NungTalungAR.Stage3.YakAI
{
    public class LaserController : MonoBehaviour
    {
        [SerializeField] private ParticleSystem PreparePS;
        [SerializeField] private ParticleSystem LaunchPS;
        [SerializeField] private AudioClip NoseBlow;
        [SerializeField] private AudioClip NoseSneeze;
        [SerializeField] private AudioSource LaserSFX;
        
        public void Start()
        {
            Destroy(this.gameObject, 5f);
            StartCoroutine(RemoveCollider());
        }

        private void OnTriggerEnter2D(Collider2D other)
        {
            if (other.gameObject.CompareTag("Player"))
            {
                other.GetComponent<PlayerController>().ChangeHealth(-1);
            }
        }
        
        IEnumerator RemoveCollider()
        {
            LaserSFX.clip = NoseBlow;
            LaserSFX.Play();
            yield return new WaitForSeconds(2f);
            LaunchPS.Play(); Debug.Log("Anim");
            this.GetComponentInChildren<Collider2D>().enabled = true; Debug.Log("Col-on");
            LaserSFX.clip = NoseSneeze;
            LaserSFX.Play();
            
            yield return new WaitForSeconds(.5f);
            this.GetComponentInChildren<Collider2D>().enabled = false; Debug.Log("Col-Off");
        }
    }
}