﻿using System;
using BallMinigame;
using Photon.Pun;
using UnityEngine;
using Random = UnityEngine.Random;
using Photon.Realtime;
using ExitGames.Client.Photon;
using UnityEngine.UI;
using NungTalungAR.Stage3.Player;

namespace NungTalungAR.Stage3.YakAI
{
    public class YakAI : MonoBehaviourPun, IOnEventCallback, IPunObservable
    {
        #region Setting

        [Header("Setting")] public float MAX_HP = 1000;
        public float REMAIN_HP;
        public State _state = State.Transition;
        public bool stateChanged = false;
        public int CurrentThreshold = 0;
        public int timebeforefirstmove = 5;
        public Slider BOSS_GUI;

        private Collider2D _collider2D;

        [SerializeField] private Sprite[] YakMood;
        private PunNetworkManager _punNetworkManager;
        [SerializeField] private AudioClip Laugh;
        [SerializeField] private AudioClip LaughWin;
        [SerializeField] private AudioClip VillagerWin;
        [SerializeField] private AudioSource BossSFX;
        
        public enum State
        {
            Transition,
            RandomLaser,
            FadeOff,
            FireBall
        }

        #endregion

        /* --------------------------------------------------*/

        #region RandomLaser

        [Header("RandomLaser")] public GameObject laserPrefeb;
        public int[] laserCount = new int[6] {1, 5, 5, 3, 1, 0};
        public float[] laserDelays = new float[6] {3, 3, 3, 3, 2, 0};
        public float[] UFOSpeed = new float[6] {6f, 9f, 12f, 15f, 15f, 0f};

        private int currentLaserCount = 0;
        private float nextLaserShot = 3;
        private float laserActiveTime = 1;
        private Transform randomPickedTraget;

        private const byte createLaserEventCode = 1;
        private const byte createFadeOffFireBallEventCode = 2;
        private const byte createFireBallEventCode = 3;
        private const byte createSummonMinionEventCode = 4;

        #endregion

        /* --------------------------------------------------*/

        #region FadeOffAttack

        [Header("FadeOffAttack")] 
        public GameObject FireBallPrefab;
        public float[] FireBallsVelocity = new float[6] {8f, 10f, 12f, 15f, 15f, 0f};
        public bool IsFadeOffFinished;
        public float[] FadeOffFireballDelay = new float[6] {1f, 1f, 1f, 0.8f, 0.8f, 0f};
        public int fadeOffState = 0;

        private Vector3 savedBeforeFadeOff;
        private float nextFadeOffAction;
        private int currentNumOfFadeOffBalls = 0;

        #endregion

        /* --------------------------------------------------*/

        #region FireBallAttack

        [Header("FireBallAttack")]
        public GameObject FireBall2Prefab;
        public int[] MaxAttackCount = new int[6] {1, 1, 5, 8, 1, 0};
        private int currentNumOfFireBall = 0;
        public float[] FireBallDelay = new float[6] {1, 1, .5f, .3f, 1, 0};
        public int[] MaxBounceCount = new int[6] {1, 1, 1, 2, 1, 0};
        public int fireBallState = 0;

        private float NextFireBall;
        private short randomStartSide;
        private bool IsFireBallFinished;

        #endregion

        /* --------------------------------------------------*/
        
        /* #region Minion

        [Header("Minion")]
        public GameObject MinionPrefab;
        public float[] SummonDelay = new float[6] {20, 20, 20, 15, 10, 0};

        private float NextSummon = 0;

        #endregion */
        
        /* --------------------------------------------------*/

        private void Start()
        {
            _collider2D = this.gameObject.GetComponent<Collider2D>();
            BOSS_GUI = GameObject.Find("HPSlider").GetComponent<Slider>();
            BOSS_GUI.maxValue = MAX_HP;
            BOSS_GUI.value = REMAIN_HP;
            _punNetworkManager = GameObject.FindObjectOfType<PunNetworkManager>();
        }

        private void Update()
        {
            if (_punNetworkManager.isGiantWin)
            {
                gameObject.GetComponent<SpriteRenderer>().sprite = YakMood[0];
                if (BossSFX.clip != LaughWin)
                {
                    BossSFX.clip = LaughWin;
                    BossSFX.Play();
                }

                return;
            }

            if (REMAIN_HP <= 0)
            {
                gameObject.GetComponent<SpriteRenderer>().sprite = YakMood[2];
                if (BossSFX.clip != VillagerWin)
                {
                    BossSFX.clip = VillagerWin;
                    BossSFX.Play();
                }
                
                return;
            }


            if (transform.rotation != Quaternion.Euler(0, 0, 0))
            {
                transform.rotation =
                    Quaternion.Lerp(transform.rotation, Quaternion.Euler(0, 0, 0), Time.deltaTime * 15);
            }

            if (PhotonNetwork.IsMasterClient != true)
                return;

            switch (_state)
            {
                case State.Transition:
                    Transition();
                    break;
                case State.RandomLaser:
                    RandomLaser();
                    break;
                case State.FadeOff:
                    FadeOff();
                    break;
                case State.FireBall:
                    FireBall();
                    break;
                default:
                    Transition();
                    break;
            }

            /*if (Threshold() >= 1 && Threshold() < 5)
            {
                if (Time.time > NextSummon)
                {
                    NextSummon = Time.time + SummonDelay[Threshold()];
                    SummonMinionRiseEvent();
                }
            }*/
        }

        public void OnEnable()
        {
            PhotonNetwork.AddCallbackTarget(this);
        }

        public void OnDisable()
        {
            PhotonNetwork.RemoveCallbackTarget(this);
        }

        int Threshold()
        {
            float threshold = REMAIN_HP / MAX_HP * 100;
            //Debug.Log("Debug.Log: " + threshold);
            if (threshold > 80)
            {
                CurrentThreshold = 0;
                return 0;
            }
            else if (threshold > 50)
            {
                CurrentThreshold = 1;
                return 1;
            }
            else if (threshold > 30)
            {
                CurrentThreshold = 2;
                return 2;
            }
            else if (threshold > 5)
            {
                CurrentThreshold = 3;
                return 3;
            }
            else if (threshold > 0)
            {
                CurrentThreshold = 4;
                return 4;
            }
            else return 5;
        }

        void Transition()
        {
            if (Time.timeSinceLevelLoad > timebeforefirstmove)
            {
                _state = State.RandomLaser;
            }
        }

        void RandomLaser()
        {
            if (stateChanged)
            {
                PlayerController[] temp = GameObject.FindObjectsOfType<PlayerController>();
                randomPickedTraget = temp[Random.Range(0, temp.Length)].transform;
                nextLaserShot = Time.time + laserDelays[Threshold()];
                laserActiveTime = 2.5f;
                stateChanged = false;
            }

            if (Time.time < nextLaserShot && randomPickedTraget != null)
            {
                this.transform.position = Vector3.MoveTowards(
                    new Vector3(transform.position.x, transform.position.y, transform.position.z),
                    new Vector3(randomPickedTraget.transform.position.x, transform.position.y, transform.position.z),
                    UFOSpeed[Threshold()] * Time.deltaTime);
            }
            else if (Time.time >= nextLaserShot && randomPickedTraget != null)
            {
                randomPickedTraget = null;
                LaserRaiseEvent();
            }
            else if (laserActiveTime > 0)
            {
                laserActiveTime -= Time.deltaTime;
            }
            else if (laserActiveTime <= 0)
            {
                currentLaserCount++;
                PlayerController[] temp = GameObject.FindObjectsOfType<PlayerController>();
                if (temp.Length != 0)
                {
                    randomPickedTraget = temp[Random.Range(0, temp.Length)].transform;
                }

                nextLaserShot = Time.time + laserDelays[Threshold()];
                laserActiveTime = 2.5f;
            }

            if (currentLaserCount >= laserCount[Threshold()])
            {
                currentLaserCount = 0;
                switch (Threshold())
                {
                    case 0: break;
                    case 1:
                        _state = State.FadeOff;
                        stateChanged = true;
                        break;
                    case 2:
                        _state = State.FireBall;
                        stateChanged = true;
                        break;
                    case 3:
                        _state = State.FireBall;
                        stateChanged = true;
                        break;
                    case 4: break;
                    case 5: break;
                }

                //Debug.Log("Switch mechanic state from RandomLaser to " + _state.ToString());
                //Debug.Log("Threshold: "+ Threshold());
            }
        }

        private void LaserRaiseEvent()
        {
            object[] content = new object[] {new Vector3(2.260577f, 2.260577f, 2.260577f)};
            RaiseEventOptions raiseEventOptions = new RaiseEventOptions {Receivers = ReceiverGroup.All};
            SendOptions sendOptions = new SendOptions {Reliability = true, Encrypt = true};
            PhotonNetwork.RaiseEvent(createLaserEventCode, content, raiseEventOptions, sendOptions);
        }

        void FadeOff()
        {
            if (stateChanged)
            {
                nextFadeOffAction = Time.time + FadeOffFireballDelay[Threshold()];
                fadeOffState = 0;
                stateChanged = false;
                currentNumOfFadeOffBalls = 0;
                BossSFX.clip = Laugh;
                BossSFX.Play();
            }

            if (Time.time > nextFadeOffAction && fadeOffState == 0)
            {
                savedBeforeFadeOff = this.transform.position;
                fadeOffState = 1;
            }
            else if (fadeOffState == 1)
            {
                this.transform.position = Vector2.MoveTowards(
                    this.transform.position,
                    savedBeforeFadeOff + Vector3.up * 5,
                    FadeOffFireballDelay[Threshold()]);

                if (this.transform.position == savedBeforeFadeOff + Vector3.up * 5)
                {
                    _collider2D.enabled = false;
                    fadeOffState = 2;
                    this.transform.position = new Vector3(0, savedBeforeFadeOff.y + 5, 0);
                }
            }
            else if (fadeOffState == 2)
            {
                if (Time.time > nextFadeOffAction)
                {
                    nextFadeOffAction = Time.time + FadeOffFireballDelay[Threshold()];
                    FadeOffRiseEvent();
                }

                if (currentNumOfFadeOffBalls > 4)
                {
                    fadeOffState = 3;
                    nextFadeOffAction = Time.time + 3f;
                }
            }
            else if (Time.time > nextFadeOffAction && fadeOffState == 3)
            {
                _collider2D.enabled = true;
                this.transform.position = Vector3.MoveTowards(this.transform.position,
                    new Vector3(0, savedBeforeFadeOff.y - 10, 0),
                    100 * Time.deltaTime);
                if (this.transform.position == new Vector3(0, savedBeforeFadeOff.y - 10, 0))
                {
                    fadeOffState = 4;
                }
            }
            else if (fadeOffState == 4)
            {
                this.transform.position = Vector3.MoveTowards(this.transform.position, savedBeforeFadeOff,
                    FireBallsVelocity[Threshold()] * Time.deltaTime);
                if (this.transform.position == savedBeforeFadeOff)
                {
                    fadeOffState = 0;
                    IsFadeOffFinished = true;
                }
            }

            if (IsFadeOffFinished)
            {
                switch (Threshold())
                {
                    case 0: break;
                    case 1:
                        _state = State.RandomLaser;
                        stateChanged = true;
                        break;
                    case 2:
                        _state = State.RandomLaser;
                        stateChanged = true;
                        break;
                    case 3:
                        _state = State.RandomLaser;
                        stateChanged = true;
                        break;
                    case 4: break;
                    case 5: break;
                }

                IsFadeOffFinished = false;
            }
        }

        private void FadeOffRiseEvent()
        {
            float FadeOffFireBallSpawnPos = 0;
            switch (currentNumOfFadeOffBalls)
            {
                case 0:
                    FadeOffFireBallSpawnPos = -10;
                    break;
                case 1:
                    FadeOffFireBallSpawnPos = -5;
                    break;
                case 2:
                    FadeOffFireBallSpawnPos = 0;
                    break;
                case 3:
                    FadeOffFireBallSpawnPos = 5;
                    break;
                case 4:
                    FadeOffFireBallSpawnPos = 10;
                    break;
            }

            object[] content = new object[]
                {new Vector3(FadeOffFireBallSpawnPos, 7.45f, 0f), FireBallsVelocity[Threshold()]};
            RaiseEventOptions raiseEventOptions = new RaiseEventOptions {Receivers = ReceiverGroup.All};
            SendOptions sendOptions = new SendOptions {Reliability = true, Encrypt = true};
            PhotonNetwork.RaiseEvent(createFadeOffFireBallEventCode, content, raiseEventOptions, sendOptions);
            currentNumOfFadeOffBalls++;
        }

        void FireBall()
        {
            if (stateChanged)
            {
                fireBallState = 0;
                stateChanged = false;
                currentNumOfFireBall = 0;
                BossSFX.clip = Laugh;
                BossSFX.Play();
            }
            else if (fireBallState == 0)
            {
                this.transform.position = Vector3.MoveTowards(this.transform.position,
                    new Vector3(0, 8.45f, 0),
                    8 * Time.deltaTime);
                if (this.transform.position == new Vector3(0, 8.45f, 0))
                {
                    fireBallState = 1;
                    NextFireBall = Time.time + FireBallDelay[Threshold()];
                    randomStartSide = (short) Random.Range(0, 2);
                }
            }
            else if (fireBallState == 1 && Time.time > NextFireBall)
            {
                if (Time.time > NextFireBall)
                {
                    NextFireBall = Time.time + FireBallDelay[Threshold()];
                    FireBallRiseEvent();
                }

                if (currentNumOfFireBall > MaxAttackCount[Threshold()])
                {
                    fireBallState = 2;
                    NextFireBall = Time.time + 3f;
                }
            }
            else if (fireBallState == 2 && Time.time > NextFireBall)
            {
                fireBallState = 0;
                IsFireBallFinished = true;
            }

            if (IsFireBallFinished)
            {
                Debug.Log("Running Fireball State!");
                switch (Threshold())
                {
                    case 0: break;
                    case 1: break;
                    case 2:
                        _state = State.FadeOff;
                        stateChanged = true;
                        break;
                    case 3:
                        _state = State.FadeOff;
                        stateChanged = true;
                        break;
                    case 4: break;
                    case 5: break;
                }

                IsFadeOffFinished = false;
            }
        }

        private void FireBallRiseEvent()
        {
            int fireBallSpawnPos = randomStartSide + currentNumOfFireBall;
            if (fireBallSpawnPos % 2 == 0)
            {
                fireBallSpawnPos = -10;
            }
            else
            {
                fireBallSpawnPos = 10;
            }

            object[] content = new object[]
            {
                new Vector3((fireBallSpawnPos + Random.Range(-1f, 1f)), 7.45f, 0f), MaxBounceCount[Threshold()],
                new Vector2(Random.Range(-5f, 5f), Random.Range(0f, 1f))
            };
            RaiseEventOptions raiseEventOptions = new RaiseEventOptions {Receivers = ReceiverGroup.All};
            SendOptions sendOptions = new SendOptions {Reliability = true, Encrypt = true};
            PhotonNetwork.RaiseEvent(createFireBallEventCode, content, raiseEventOptions, sendOptions);
            currentNumOfFireBall++;
        }

        private void SummonMinionRiseEvent()
        {
            short spawnSide = (short) Random.Range(0, 2);
            object[] content = new object[] {spawnSide};
            RaiseEventOptions raiseEventOptions = new RaiseEventOptions {Receivers = ReceiverGroup.All};
            SendOptions sendOptions = new SendOptions {Reliability = true, Encrypt = true};
            PhotonNetwork.RaiseEvent(createSummonMinionEventCode, content, raiseEventOptions, sendOptions);
        }

        public void OnEvent(EventData photonEvent)
        {
            //Debug.Log(photonEvent.ToStringFull());
            byte eventCode = photonEvent.Code;

            if (eventCode == createLaserEventCode)
            {
                //Debug.Log("Call Raise Event is : " + eventCode.ToString());
                object[] data = (object[]) photonEvent.CustomData;

                Vector3 LaserScale = (Vector3) data[0];

                GameObject spawnedLaser = Instantiate(laserPrefeb, this.transform.position + Vector3.down * 4.5f, Quaternion.identity);
                spawnedLaser.transform.localScale = LaserScale;
            }

            if (eventCode == createFadeOffFireBallEventCode)
            {
                if (PhotonNetwork.IsMasterClient)
                {
                    //Debug.Log("Call Raise Event is : " + eventCode.ToString());
                    object[] data = (object[]) photonEvent.CustomData;

                    Vector3 FireBallPos = (Vector3) data[0];
                    float FireBallVelocity = (float) data[1];

                    GameObject spawnedFireBall =
                        PhotonNetwork.Instantiate(FireBallPrefab.name, FireBallPos, Quaternion.identity);
                    spawnedFireBall.GetComponent<FadeOffFireBallController>().velocity = FireBallVelocity;
                }
            }

            if (eventCode == createFireBallEventCode)
            {
                if (PhotonNetwork.IsMasterClient)
                {
                    //Debug.Log("Call Raise Event is : " + eventCode.ToString());
                    object[] data = (object[]) photonEvent.CustomData;

                    Vector3 FireBallPos = (Vector3) data[0];
                    int BounceCount = (int) data[1];
                    Vector2 upForce = (Vector2) data[2];

                    GameObject spawnedFireBall =
                        PhotonNetwork.Instantiate(FireBall2Prefab.name, FireBallPos, Quaternion.identity);
                    spawnedFireBall.GetComponent<FireBallController>().BounceCount = BounceCount;
                    spawnedFireBall.GetComponent<FireBallController>().upForce = upForce;
                }
            }

            /*if (eventCode == createSummonMinionEventCode)
            {
                if (PhotonNetwork.IsMasterClient)
                {
                    //Debug.Log("Call Raise Event is : " + eventCode.ToString());
                    object[] data = (object[]) photonEvent.CustomData;

                    short spawnside = (short) data[0];

                    Vector2 spawnPos = Vector2.zero;

                    if (spawnside == 0)
                    {
                        spawnPos = new Vector2(-30, 5);
                    }
                    else
                    {
                        spawnPos = new Vector2(30, 5);
                    }

                    PhotonNetwork.Instantiate(MinionPrefab.name, spawnPos, Quaternion.identity);
                }
            }*/
        }

        public void ChangeHealth(int amount)
        {
            if (photonView != null)
                photonView.RPC("RPCChangeHealth", RpcTarget.All, amount);
            
            
        }

        [PunRPC]
        public void RPCChangeHealth(int amount)
        {
            REMAIN_HP += amount;
            Debug.Log(this.name + "'s remaining hp = " + REMAIN_HP);
            if (PhotonNetwork.IsMasterClient)
            {
                if (REMAIN_HP <= 0)
                {
                    Rigidbody2D rb = this.gameObject.GetComponent<Rigidbody2D>();
                    rb.constraints = RigidbodyConstraints2D.None;
                    Destroy(gameObject, 10);
                }
            }
        }
        public void OnPhotonSerializeView(PhotonStream stream, PhotonMessageInfo info)
        {
            /*
            if (stream.IsWriting)
            {
                stream.SendNext((short) REMAIN_HP);
            }
            else
            {
                REMAIN_HP = (short) stream.ReceiveNext();
            }
            */
        }

        private void LateUpdate()
        {
            BOSS_GUI.value = REMAIN_HP;
        }

        private void OnCollisionEnter2D(Collision2D other)
        {
            if (other.gameObject.CompareTag("Player"))
            {
                other.gameObject.GetComponent<PlayerController>().ChangeHealth(-1);
            }
        }

        private void OnDestroy()
        {
            if (!photonView.IsMine)
                return;

            PhotonNetwork.Destroy(this.gameObject);
        }
    }
}