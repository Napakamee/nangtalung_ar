﻿using System;
using System.Collections;
using System.Collections.Generic;
using Photon.Pun;
using UnityEngine;

public class RahulSpawner : MonoBehaviourPunCallbacks
{
    [SerializeField] private GameObject RahulPrefabs;
    private void Start()
    {
        if (!PhotonNetwork.IsMasterClient)
            return;

        PhotonNetwork.InstantiateRoomObject(RahulPrefabs.name, RahulPrefabs.transform.position, Quaternion.identity, 0);
    }
}
