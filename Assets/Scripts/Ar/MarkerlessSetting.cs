﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using OpenCVMarkerLessAR;
using OpenCVForUnity.CoreModule;
using OpenCVForUnity.ImgcodecsModule;
using OpenCVForUnity.ImgprocModule;
using OpenCVForUnity.UnityUtils;
using OpenCVForUnity.Calib3dModule;
using OpenCVForUnity.UnityUtils.Helper;


public class MarkerlessSetting : MonoBehaviour
{
    public WebCamTextureMarkerLessAR webCamTextureMarkerLessAR { get; set; }

    /// <summary>
    /// The AR game object.
    /// </summary>
    public GameObject ARGameObject;

    /// <summary>
    /// The AR camera.
    /// </summary>
    public Camera ARCamera;

    /// <summary>
    /// The cube.
    /// </summary>
    public GameObject cube;

    /// <summary>
    /// Determines if should move AR camera.
    /// </summary>
    public bool shouldMoveARCamera;

    public bool flipCheck;

    public bool isFlip;
    public bool isMirror;


    /// <summary>
    /// The dist coeffs.
    /// </summary>
    MatOfDouble distCoeffs;



    /// <summary>
    /// The matrix that inverts the Y axis.
    /// </summary>
    Matrix4x4 invertYM;

    /// <summary>
    /// The matrix that inverts the Z axis.
    /// </summary>
    Matrix4x4 invertZM;

    /// <summary>
    /// The transformation matrix.
    /// </summary>
    Matrix4x4 transformationM;



    /// <summary>
    /// The transformation matrix for AR.
    /// </summary>
    Matrix4x4 ARM;

    /// <summary>
    /// The pattern.
    /// </summary>
    Pattern pattern;

    /// <summary>
    /// The pattern.
    /// </summary>
    Pattern patternFlip;



    /// <summary>
    /// The pattern tracking info.
    /// </summary>
    PatternTrackingInfo patternTrackingInfo;

    /// <summary>
    /// The pattern detector.
    /// </summary>
    PatternDetector patternDetector;

    /// <summary>
    /// The pattern detector.
    /// </summary>
    PatternDetector patternDetectorFlip;

    /// <summary>
    /// The pattern mat.
    /// </summary>
    public Mat patternMat { get; private set; }

    [SerializeField] private string markerName = "patternImg.jpg";


    // Start is called before the first frame update
    void Start()
    {
        cube.SetActive(false);






        patternMat = Imgcodecs.imread(Application.streamingAssetsPath + "/" + markerName);

        if (patternMat.total() == 0)
        {

            return;
        }
        else
        {

            Imgproc.cvtColor(patternMat, patternMat, Imgproc.COLOR_BGR2RGB);

            Texture2D patternTexture = new Texture2D(patternMat.width(), patternMat.height(), TextureFormat.RGBA32, false);

            //To reuse mat, set the flipAfter flag to true.
            Utils.matToTexture2D(patternMat, patternTexture, true, 0, true);
            Debug.Log("patternMat dst ToString " + patternMat.ToString());


            pattern = new Pattern();
            patternTrackingInfo = new PatternTrackingInfo();

            patternDetector = new PatternDetector(null, null, null, true);

            patternDetector.buildPatternFromImage(patternMat, pattern);
            patternDetector.train(pattern);

            if (flipCheck)
            {
                patternDetectorFlip = new PatternDetector(null, null, null, true);
                patternFlip = new Pattern();
                Utils.matToTexture2D(patternMat, patternTexture, false, 0, true);
                patternDetectorFlip.buildPatternFromImage(patternMat, patternFlip);
                patternDetectorFlip.train(patternFlip);

            }



        }

        transformationM = new Matrix4x4();

        invertYM = Matrix4x4.TRS(Vector3.zero, Quaternion.identity, new Vector3(1, -1, 1));
        Debug.Log("invertYM " + invertYM.ToString());

        invertZM = Matrix4x4.TRS(Vector3.zero, Quaternion.identity, new Vector3(1, 1, -1));
        Debug.Log("invertZM " + invertZM.ToString());

        distCoeffs = new MatOfDouble(0, 0, 0, 0);
        Debug.Log("distCoeffs " + distCoeffs.dump());


    }

    void Update()
    {
        if (webCamTextureMarkerLessAR.webCamTextureToMatHelper.IsPlaying() && webCamTextureMarkerLessAR.webCamTextureToMatHelper.DidUpdateThisFrame())
        {

            Mat rgbaMat = webCamTextureMarkerLessAR.webCamTextureToMatHelper.GetMat();

            Imgproc.cvtColor(rgbaMat, webCamTextureMarkerLessAR.grayMat, Imgproc.COLOR_RGBA2GRAY);


            bool patternFound = patternDetector.findPattern(webCamTextureMarkerLessAR.grayMat, patternTrackingInfo);
            if (flipCheck) if (!patternFound)
                {
                    patternFound = patternDetectorFlip.findPattern(webCamTextureMarkerLessAR.grayMat, patternTrackingInfo);
                    isFlip = patternFound;
                }


            if (patternFound)
            {

                patternTrackingInfo.computePose(pattern, webCamTextureMarkerLessAR.camMatrix, distCoeffs);

                //Marker to Camera Coordinate System Convert Matrix
                transformationM = patternTrackingInfo.pose3d;
                //Debug.Log ("transformationM " + transformationM.ToString ());

                if (shouldMoveARCamera)
                {
                    ARM = ARGameObject.transform.localToWorldMatrix * invertZM * transformationM.inverse * invertYM;
                    //Debug.Log ("ARM " + ARM.ToString ());

                    ARUtils.SetTransformFromMatrix(ARCamera.transform, ref ARM);

                }
                else
                {

                    ARM = ARCamera.transform.localToWorldMatrix * invertYM * transformationM * invertZM;
                    //Debug.Log ("ARM " + ARM.ToString ());

                    ARUtils.SetTransformFromMatrix(ARGameObject.transform, ref ARM);

                    Vector3 position = ARGameObject.transform.position;
                    position.x = isMirror? -position.x : position.x ;
                    position.z = 0;
                    ARGameObject.transform.position = position;

                    Vector3 eulerAngles = ARGameObject.transform.eulerAngles;
                    eulerAngles.x = 0;
                    eulerAngles.y = 0;
                    ARGameObject.transform.eulerAngles = eulerAngles;

                    Vector3 scale = ARGameObject.transform.localScale;
                    scale.y = scale.z * (isFlip ? 1 : -1);
                    ARGameObject.transform.localScale = scale;
                }

                cube.SetActive(true);
                //ARGameObject.GetComponent<DelayableSetActive>().SetActive(true);
            }
            else
            {
                cube.SetActive(false);
                //ARGameObject.GetComponent<DelayableSetActive>().SetActive(false, 0.5f);
            }

            Utils.fastMatToTexture2D(rgbaMat, webCamTextureMarkerLessAR.texture);
        }
    }
}
