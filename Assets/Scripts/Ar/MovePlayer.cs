﻿using System;
using System.Collections;
using System.Collections.Generic;
using Photon.Pun;
using UnityEngine;
using UnityEngine.UI;

public class MovePlayer : MonoBehaviourPun
{

    public Transform arTarget;
    public Transform player;
    public RectTransform ghostTarget;
    public GameObject playerObj;
    public GameObject ghostImage;
    private GameObject findPlayer = null;

    [SerializeField] private float scaling = 1;
    [SerializeField] private bool isFacing;
    [SerializeField] private bool isRotate;

    [SerializeField] public bool isMirror;
    [SerializeField] private Vector3 offset;

    [SerializeField] private bool LockYMovement;

    [Header("Keyboard Zone")] 
    public bool usingKeyboard = false;
    public bool facingRight = false;
    private Vector3 movement;
    private Vector2 moveDirection;
    private Rigidbody2D rb;
    [SerializeField]
    private float moveSpeed = 5f;

    private RectTransform transform;

    public float smoothTime = 0.3F;
    private Vector3 velocity = Vector3.zero;
    private Vector3 targetPosition;

    private float prevX;
    private float scaleX;
    private float textScaleX;

    public void Start()
    {
        GetPlayerPref();
    }

    public void GetPlayerPref()
    {
        //player didn't join yet
        if (player == null)
        {
            findPlayer = PunUserNetControl.LocalPlayerInstance;
            playerObj = findPlayer.transform.Find("Parent").gameObject;
            player = findPlayer.transform;
                
            ghostImage = playerObj.transform.Find("Canvas").gameObject;
            ghostImage = ghostImage.transform.Find("GhostImage").gameObject;
            ghostTarget = ghostImage.GetComponent<RectTransform>();
            rb = findPlayer.GetComponent<Rigidbody2D>();
        }
    }
    // Update is called once per frame
    void Update()
    {
        if (PunUserNetControl.isPlayerJoined)
        {
            GetPlayerPref();
        }
        movement.x = Input.GetAxisRaw("Horizontal");
        movement.y = Input.GetAxisRaw("Vertical");
        if (Input.GetKey(KeyCode.LeftArrow) || Input.GetKey(KeyCode.RightArrow))
        {
            usingKeyboard = true;
        }else if (Input.GetKeyUp(KeyCode.LeftArrow) || Input.GetKeyUp(KeyCode.RightArrow))
        {
            usingKeyboard = false;
        }
        moveDirection = new Vector2(movement.x, movement.y).normalized;
        

    }

    private void FixedUpdate()
    {
        MoveCharacter();
    }

    private void MoveCharacter()
    {
        if (LockYMovement)
        {
            rb.velocity = new Vector2(moveDirection.x * moveSpeed, rb.velocity.y);
        }
        else
        {
            rb.velocity = new Vector2(moveDirection.x * moveSpeed, moveDirection.y * moveSpeed);
            ghostTarget.Translate(new Vector3(moveDirection.x * moveSpeed * 30 * Time.fixedDeltaTime, moveDirection.y * moveSpeed * 30 * Time.fixedDeltaTime));
        }

        if (arTarget.gameObject.activeSelf)
        {
            Vector3 pos = arTarget.transform.localPosition;
            pos.z = 0;
            pos.x = isMirror ? -pos.x : pos.x;
            //rectTransform.localPosition = pos * scaling;
            targetPosition = pos * scaling;

            if (!findPlayer.CompareTag("PlayerDeath"))
            {
                player.localPosition = Vector3.SmoothDamp(player.localPosition, targetPosition + offset, ref velocity, smoothTime);
            }
            else if(findPlayer.CompareTag("PlayerDeath"))
            {
                targetPosition = pos * scaling * 30;
                ghostTarget.localPosition = Vector3.SmoothDamp(ghostTarget.localPosition, targetPosition + offset, ref velocity, smoothTime);
            }
            else if (findPlayer.CompareTag("Spectator"))
            {
                
            }
            
        }
        
        if(!isRotate)return;
        
        Vector3 playerScale = playerObj.transform.localScale;
        if (moveDirection.x > 0 && !facingRight && usingKeyboard)
        {
            facingRight = true;
            playerScale.x = -1;
            playerObj.transform.localScale = playerScale;
            ghostImage.transform.localScale = playerScale;
        }
        if(moveDirection.x < 0 && facingRight && usingKeyboard)
        {
            facingRight = false;
            playerScale.x = 1;
            playerObj.transform.localScale = playerScale;
            ghostImage.transform.localScale = playerScale;
        }
        
        if (arTarget.transform.localPosition.x < 0 && !usingKeyboard)
        {
            playerScale.x = -1;
            playerObj.transform.localScale = playerScale;
            ghostImage.transform.localScale = playerScale;
        }
        else if(arTarget.transform.localPosition.x > 0 && !usingKeyboard)
        {
            playerScale.x = 1;
            playerObj.transform.localScale = playerScale;
            ghostImage.transform.localScale = playerScale;
            //Debug.Log("player scale: " + playerScale.x);
        }
    }
}
