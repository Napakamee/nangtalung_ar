﻿using System;
using System.Collections;
using System.Collections.Generic;
using NUnit.Framework.Constraints;
using Photon.Pun;
using UnityEngine;
using UnityEngine.UI;

public class MoveTargetIK : MonoBehaviourPun
{

    [SerializeField] public Transform target;
    [SerializeField] private Transform parent;
    [SerializeField] private float scaling = 1;
    [SerializeField] private bool isRotate;

    [SerializeField] public bool isMirror;
    [SerializeField] private Vector3 offset;

    private GameObject findPlayer = null;
    private GameObject findARObject = null;
    [SerializeField] private bool  ARObjectFound = false;

    [SerializeField] private string TargetName = "ARGameObjectHand";

    [Header("Keyboard Zone")]
    private Vector3 movement;
    private Vector2 moveDirection;
    private Rigidbody2D rb;
    [SerializeField]
    private float moveSpeed = 5f;
    
    private Transform transform;

    public float smoothTime = 0.3F;
    private Vector3 velocity = Vector3.zero;
    private Vector3 targetPosition;

    private float prevX;
    private float scaleX;


    
    // Start is called before the first frame update
    void Start()
    {
        rb = GetComponent<Rigidbody2D>();
        transform = this.GetComponent<Transform>();
        scaleX = transform.localScale.x;
        
    }

    public void FindARObject()
    {
        findARObject = GameObject.Find(TargetName);
        target = findARObject.transform;
        ARObjectFound = true;
    }

    private void FixedUpdate()
    {
        if (!photonView.IsMine)
            return;
        Vector3 dir = moveDirection;
        dir.x = isMirror ? dir.x : -dir.x;
        transform.Translate( new Vector3(dir.x * moveSpeed * Time.fixedDeltaTime, moveDirection.y * moveSpeed * Time.fixedDeltaTime));
        
    }

    // Update is called once per frame
    void Update()
    {
        if (!photonView.IsMine)
            return;
        if (!ARObjectFound && CheckARObjectActive.ISObjectActive)
        {
            FindARObject();
        }

        movement.x = Input.GetAxisRaw("HorizontalIK");
        movement.y = Input.GetAxisRaw("VerticalIK");
        moveDirection = new Vector2(movement.x, movement.y).normalized;
        
        if (parent.localScale.x < 0)
        {
            isMirror = false;
        }
        if (parent.localScale.x > 0)
        {
            isMirror = true;
        }
        
        if (ARObjectFound)
        {
            if (target.gameObject.activeSelf)
            {
                Vector3 pos = target.transform.localPosition;
                pos.z = 0;
                pos.x = isMirror ? -pos.x : pos.x;
                //rectTransform.localPosition = pos * scaling;
                targetPosition = pos * scaling;

                transform.localPosition = Vector3.SmoothDamp(transform.localPosition, targetPosition + offset, ref velocity, smoothTime);
            }
            
        }
        
        if(!isRotate)return;
    }
    
}
