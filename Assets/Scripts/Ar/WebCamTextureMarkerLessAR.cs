﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using OpenCVMarkerLessAR;
using OpenCVForUnity.CoreModule;
using OpenCVForUnity.ImgcodecsModule;
using OpenCVForUnity.ImgprocModule;
using OpenCVForUnity.UnityUtils;
using OpenCVForUnity.Calib3dModule;
using OpenCVForUnity.UnityUtils.Helper;


/// <summary>
/// WebCamTexture Markerless AR Example
/// This code is a rewrite of https://github.com/MasteringOpenCV/code/tree/master/Chapter3_MarkerlessAR using "OpenCV for Unity".
/// </summary>
[RequireComponent(typeof(WebCamTextureToMatHelper))]
public class WebCamTextureMarkerLessAR : MonoBehaviour
{



    /// <summary>
    /// The texture.
    /// </summary>
    public Texture2D texture{ get; private set; }

    /// <summary>
    /// The webcam texture to mat helper.
    /// </summary>
    public WebCamTextureToMatHelper webCamTextureToMatHelper { get; private set; }



    /// <summary>
    /// The gray mat.
    /// </summary>
    public Mat grayMat { get; private set; }


    /// <summary>
    /// The cameraparam matrix.
    /// </summary>
    public Mat camMatrix { get; private set; }


    /// <summary>
    /// The dist coeffs.
    /// </summary>
    MatOfDouble distCoeffs;


    /// <summary>
    /// The matrix that inverts the Y axis.
    /// </summary>
    Matrix4x4 invertYM;

    /// <summary>
    /// The matrix that inverts the Z axis.
    /// </summary>
    Matrix4x4 invertZM;

    /// <summary>
    /// The transformation matrix.
    /// </summary>
    Matrix4x4 transformationM;


    public MarkerlessSetting[] markerlessSettings;


    // Use this for initialization
    void Start()
    {
        for (int i = 0; i < markerlessSettings.Length; i++)
        {
            markerlessSettings[i].webCamTextureMarkerLessAR = this;
        }

        webCamTextureToMatHelper = gameObject.GetComponent<WebCamTextureToMatHelper>();
#if UNITY_ANDROID && !UNITY_EDITOR
                // Avoids the front camera low light issue that occurs in only some Android devices (e.g. Google Pixel, Pixel2).
                webCamTextureToMatHelper.avoidAndroidFrontCameraLowLightIssue = true;
#endif
        webCamTextureToMatHelper.Initialize();

    }

    /// <summary>
    /// Raises the web cam texture to mat helper initialized event.
    /// </summary>
    public void OnWebCamTextureToMatHelperInitialized()
    {
        Debug.Log("OnWebCamTextureToMatHelperInitialized");

        Mat webCamTextureMat = webCamTextureToMatHelper.GetMat();

        texture = new Texture2D(webCamTextureMat.width(), webCamTextureMat.height(), TextureFormat.RGBA32, false);
        gameObject.GetComponent<Renderer>().material.mainTexture = texture;
        //gameObject.GetComponent<RawImage>().texture = texture;
        //gameObject.GetComponent<RawImage>().material.mainTexture = texture;

        grayMat = new Mat(webCamTextureMat.rows(), webCamTextureMat.cols(), CvType.CV_8UC1);


        gameObject.transform.localScale = new Vector3(webCamTextureMat.width(), webCamTextureMat.height(), 1);

        Debug.Log("Screen.width " + Screen.width + " Screen.height " + Screen.height + " Screen.orientation " + Screen.orientation);


        float width = webCamTextureMat.width();
        float height = webCamTextureMat.height();

        float imageSizeScale = 1.0f;
        float widthScale = (float)Screen.width / width;
        float heightScale = (float)Screen.height / height;
        if (widthScale < heightScale)
        {
            Camera.main.orthographicSize = (width * (float)Screen.height / (float)Screen.width) / 2;
            imageSizeScale = (float)Screen.height / (float)Screen.width;
        }
        else
        {
            Camera.main.orthographicSize = height / 2;
        }


        //set cameraparam
        int max_d = (int)Mathf.Max(width, height);
        double fx = max_d;
        double fy = max_d;
        double cx = width / 2.0f;
        double cy = height / 2.0f;
        camMatrix = new Mat(3, 3, CvType.CV_64FC1);
        camMatrix.put(0, 0, fx);
        camMatrix.put(0, 1, 0);
        camMatrix.put(0, 2, cx);
        camMatrix.put(1, 0, 0);
        camMatrix.put(1, 1, fy);
        camMatrix.put(1, 2, cy);
        camMatrix.put(2, 0, 0);
        camMatrix.put(2, 1, 0);
        camMatrix.put(2, 2, 1.0f);
        Debug.Log("camMatrix " + camMatrix.dump());


        distCoeffs = new MatOfDouble(0, 0, 0, 0);
        Debug.Log("distCoeffs " + distCoeffs.dump());


        //calibration camera
        Size imageSize = new Size(width * imageSizeScale, height * imageSizeScale);
        double apertureWidth = 0;
        double apertureHeight = 0;
        double[] fovx = new double[1];
        double[] fovy = new double[1];
        double[] focalLength = new double[1];
        Point principalPoint = new Point(0, 0);
        double[] aspectratio = new double[1];

        Calib3d.calibrationMatrixValues(camMatrix, imageSize, apertureWidth, apertureHeight, fovx, fovy, focalLength, principalPoint, aspectratio);

        Debug.Log("imageSize " + imageSize.ToString());
        Debug.Log("apertureWidth " + apertureWidth);
        Debug.Log("apertureHeight " + apertureHeight);
        Debug.Log("fovx " + fovx[0]);
        Debug.Log("fovy " + fovy[0]);
        Debug.Log("focalLength " + focalLength[0]);
        Debug.Log("principalPoint " + principalPoint.ToString());
        Debug.Log("aspectratio " + aspectratio[0]);


        //To convert the difference of the FOV value of the OpenCV and Unity. 
        double fovXScale = (2.0 * Mathf.Atan((float)(imageSize.width / (2.0 * fx)))) / (Mathf.Atan2((float)cx, (float)fx) + Mathf.Atan2((float)(imageSize.width - cx), (float)fx));
        double fovYScale = (2.0 * Mathf.Atan((float)(imageSize.height / (2.0 * fy)))) / (Mathf.Atan2((float)cy, (float)fy) + Mathf.Atan2((float)(imageSize.height - cy), (float)fy));

        Debug.Log("fovXScale " + fovXScale);
        Debug.Log("fovYScale " + fovYScale);


        //Adjust Unity Camera FOV https://github.com/opencv/opencv/commit/8ed1945ccd52501f5ab22bdec6aa1f91f1e2cfd4
        if (widthScale < heightScale)
        {
            for (int i = 0; i < markerlessSettings.Length; i++)
            {
                markerlessSettings[i].ARCamera.fieldOfView = (float)(fovx[0] * fovXScale);
            }

        }
        else
        {
            for (int i = 0; i < markerlessSettings.Length; i++)
            {
                markerlessSettings[i].ARCamera.fieldOfView = (float)(fovy[0] * fovYScale);
            }


        }




        //if WebCamera is frontFaceing,flip Mat.
        if (webCamTextureToMatHelper.GetWebCamDevice().isFrontFacing)
        {
            webCamTextureToMatHelper.flipHorizontal = true;
        }
    }

    /// <summary>
    /// Raises the web cam texture to mat helper disposed event.
    /// </summary>
    public void OnWebCamTextureToMatHelperDisposed()
    {
        Debug.Log("OnWebCamTextureToMatHelperDisposed");

        if (grayMat != null)
            grayMat.Dispose();
    }

    /// <summary>
    /// Raises the web cam texture to mat helper error occurred event.
    /// </summary>
    /// <param name="errorCode">Error code.</param>
    public void OnWebCamTextureToMatHelperErrorOccurred(WebCamTextureToMatHelper.ErrorCode errorCode)
    {
        Debug.Log("OnWebCamTextureToMatHelperErrorOccurred " + errorCode);
    }

    // Update is called once per frame
    void Update()
    {

    }

    /// <summary>
    /// Raises the destroy event.
    /// </summary>
    void OnDestroy()
    {
        webCamTextureToMatHelper.Dispose();

        for (int i = 0; i < markerlessSettings.Length; i++)
        {
            if (markerlessSettings[i].patternMat != null)
                markerlessSettings[i].patternMat.Dispose();
        }


    }

    /// <summary>
    /// Raises the back button click event.
    /// </summary>
    public void OnBackButtonClick()
    {
        SceneManager.LoadScene("MarkerLessARExample");
    }

    /// <summary>
    /// Raises the play button click event.
    /// </summary>
    public void OnPlayButtonClick()
    {
        webCamTextureToMatHelper.Play();
    }

    /// <summary>
    /// Raises the pause button click event.
    /// </summary>
    public void OnPauseButtonClick()
    {
        webCamTextureToMatHelper.Pause();
    }

    /// <summary>
    /// Raises the stop button click event.
    /// </summary>
    public void OnStopButtonClick()
    {
        webCamTextureToMatHelper.Stop();
    }

    /// <summary>
    /// Raises the change camera button click event.
    /// </summary>
    public void OnChangeCameraButtonClick()
    {
        webCamTextureToMatHelper.requestedIsFrontFacing = !webCamTextureToMatHelper.IsFrontFacing();
    }







}

