﻿using System;
using System.Collections;
using System.Collections.Generic;
using OpenCVForUnity.UnityUtils.Helper;
using UnityEngine;
using UnityEngine.UI;

public class ShowWebcamTextureOnUI : MonoBehaviour
{
    public RawImage rawimage;
    [SerializeField][Tooltip("Usually on WebCam Object")] 
    private WebCamTextureToMatHelper webCamTextureToMatHelper;
    
    [SerializeField][Tooltip("AR GameObject")] 
    private GameObject ARObject;
    
    private bool finishedSetup = false;

    private void Update()
    {
        //Check if webcam works or not by checking if ARGameobject is active or not.
        //If works stop update
        if (ARObject.gameObject.activeSelf && !finishedSetup)
        {
            finishedSetup = true;
            Debug.Log("WebCam UI finished setup.");
        }
        
        if (!finishedSetup)
        {
            WebCamTexture webcamTexture = webCamTextureToMatHelper.webCamTexture;
            rawimage.texture = webcamTexture;
            rawimage.material.mainTexture = webcamTexture;
        }
        
    }
}
