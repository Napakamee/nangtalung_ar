﻿using System.Collections;
using System.Collections.Generic;
using Photon.Pun;
using UnityEngine;
using UnityEngine.UI;

public class MoveTarget : MonoBehaviourPun
{

    [SerializeField] public Transform target;
    [SerializeField] public Transform image;
    [SerializeField] public Transform text;
    [SerializeField] private float scaling = 1;
    [SerializeField] private bool isFacing;
    [SerializeField] private bool isRotate;

    [SerializeField] public bool isMirror;
    [SerializeField] private Vector3 offset;



    private RectTransform transform;

    public float smoothTime = 0.3F;
    private Vector3 velocity = Vector3.zero;
    private Vector3 targetPosition;

    private float prevX;
    private float scaleX;
    private float textScaleX;


    // Start is called before the first frame update
    void Start()
    {
        transform = this.GetComponent<RectTransform>();
        scaleX = transform.localScale.x;
        if (text != null) textScaleX = text.transform.localScale.x;
    }

    // Update is called once per frame
    void Update()
    {
        
        if (target.gameObject.activeSelf)
        {
            Vector3 pos = target.transform.localPosition;
            pos.z = 0;
             pos.x = isMirror ? -pos.x : pos.x;
            //rectTransform.localPosition = pos * scaling;
            targetPosition = pos * scaling;

            transform.localPosition = Vector3.SmoothDamp(transform.localPosition, targetPosition + offset, ref velocity, smoothTime);

        }


        /* 
        if (isFacing)
        {
            float direction = rectTransform.localPosition.x - prevX;
            prevX = rectTransform.localPosition.x;
            if (direction > -1 && direction < 1)
            {
                direction = scaleX;
            }
            else
            {
                direction = Mathf.Sign(direction) * scaleX;
            }

            Vector3 scale = rectTransform.localScale;
            scale.x = direction;
            rectTransform.localScale = scale;
        }
        */

        if(!isRotate)return;

        if (target.gameObject.activeSelf)
        {
            transform.localScale = target.transform.localScale * scaleX;

            Vector3 scale = target.transform.localScale;
            scale.x = scale.y;
            scale.y = scale.z;
            text.transform.localScale = scale * textScaleX;


             transform.eulerAngles = target.transform.eulerAngles;
        }



     



    }

    public void SetPosition()
    {
        if (transform == null) transform = this.GetComponent<RectTransform>();

        Vector3 pos = target.transform.localPosition;
        pos.z = 0;

        targetPosition = pos * scaling;
        transform.localPosition = targetPosition;

        transform.eulerAngles = target.transform.eulerAngles;


    }

}
