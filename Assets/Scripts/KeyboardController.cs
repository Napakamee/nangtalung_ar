﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class KeyboardController : MonoBehaviour
{
    public Transform player;
    public RectTransform ghostTarget;
    public GameObject playerObj;
    public GameObject ghostImage;
    private GameObject findPlayer = null;
    public bool facingRight = true;
    private Vector3 movement;
    private Vector2 moveDirection;
    private Rigidbody2D rb;
    [SerializeField]
    private float moveSpeed = 5f;
    // Start is called before the first frame update
    void Start()
    {
        
    }
    public void GetPlayerPref()
    {
        //player didn't join yet
        if (player == null)
        {
            
            findPlayer = PunUserNetControl.LocalPlayerInstance;
            playerObj = findPlayer.transform.Find("Parent").gameObject;
            player = findPlayer.transform;
                
            ghostImage = playerObj.transform.Find("Canvas").gameObject;
            ghostImage = ghostImage.transform.Find("GhostImage").gameObject;
            ghostTarget = ghostImage.GetComponent<RectTransform>();
            rb = findPlayer.GetComponent<Rigidbody2D>();

        }
    }
    // Update is called once per frame
    void Update()
    {
        if (PunUserNetControl.isPlayerJoined)
        {
            GetPlayerPref();
        }

        movement.x = Input.GetAxisRaw("Horizontal");
        movement.y = Input.GetAxisRaw("Vertical");
        if (movement.x > 0 && !facingRight)
            Flip();
        else if (movement.x < 0 && facingRight)
            Flip();
        moveDirection = new Vector2(movement.x, movement.y).normalized;
    }

    private void Flip()
    {
        facingRight = !facingRight;
        Vector3 theScale = playerObj.transform.localScale;
        theScale.x *= -1;
        playerObj.transform.localScale = theScale;
    }

    private void FixedUpdate()
    {
        MovePlayer();
    }

    private void MovePlayer()
    {
        Vector3 playerScale = playerObj.transform.localScale;
        rb.velocity = new Vector2(moveDirection.x * moveSpeed, moveDirection.y * moveSpeed);
        /*if (moveDirection.x < 0)
        {
            playerScale.x = -1;
            playerObj.transform.localScale = playerScale;
        }
        else
        {
            playerScale.x = 1;
            playerObj.transform.localScale = playerScale;
        }*/
        //Debug.Log(moveDirection.x);
    }
}
