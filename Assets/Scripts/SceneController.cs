﻿using System.Collections;
using System.Collections.Generic;
using Photon.Pun;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SceneController : MonoBehaviourPun
{
    public void ChangeScene(string sceneToLoad)
    {
        if (PhotonNetwork.NetworkClientState == Photon.Realtime.ClientState.Joined)
        {
            PhotonNetwork.LeaveRoom();
            //PhotonNetwork.Disconnect();
        }

        PunUserNetControl.isPlayerJoined = false;
        PunUserNetControl.LocalPlayerInstance = null;
        SceneManager.UnloadSceneAsync(SceneManager.GetActiveScene().name);
        SceneManager.LoadScene("Lobby");
    }
}
