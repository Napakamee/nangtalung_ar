﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class JoystickController : MonoBehaviour
{
    [Tooltip("Thing to move.")]
    public Transform player;
    public RectTransform ghost;
    public GameObject playerObj;
    public GameObject ghostImage;
    public float speed = 5.0f;
    private bool touchStart = false;
    
    private Vector2 pointA;
    private Vector2 pointB;
    //Gameobjects
    private GameObject findPlayer = null;
    private Transform findHand = null;
    
    [Tooltip("Name of hand object.")]
    public string handName = "Hand";
    [Tooltip("Joystick controller.")]
    public Transform circle;
    
    [Tooltip("Check if controller is for hand or not.")]
    [SerializeField] private bool isHand = false;
    [Tooltip("ARGameobject")]
     public Transform ARTarget;

     [Tooltip("Check if Player is moving in sidescroll or not")] [SerializeField]
     private bool IsSideScroll = false;

     private void Awake()
     {
         
         pointA = circle.localPosition;
     }

     //use to find local player gameobject
    public void GetPlayerPref()
    {
        //player didn't join yet
        if (player == null)
        {
            //check is this controller is for hand
            if(!isHand)
            {
                findPlayer = PunUserNetControl.LocalPlayerInstance;
                playerObj = findPlayer.transform.Find("Parent").gameObject;
                player = findPlayer.transform;
                
                ghostImage = playerObj.transform.Find("Canvas").gameObject;
                ghostImage = ghostImage.transform.Find("GhostImage").gameObject;
                ghost = ghostImage.GetComponent<RectTransform>();
            }
        }
    }
    // Update is called once per frame
    void Update () {
        if (ARTarget.gameObject.activeSelf)
        {
            touchStart = true;
            pointB = new Vector2(ARTarget.position.x, ARTarget.position.y);
        }
        else
        {
            touchStart = false;
        }
        
        if (PunUserNetControl.isPlayerJoined)
        {
            GetPlayerPref();
        }
    }
    private void FixedUpdate(){
        if(touchStart){
            Vector2 offset = pointB - pointA;
            Vector2 direction = Vector2.ClampMagnitude(offset, 1.0f);
            MoveCharacter(new Vector3(direction.x *-1, direction.y,1));

            //circle.transform.position = new Vector2(pointA.x + direction.x, pointA.y + direction.y) * -1;
        }
        else
        {
            circle.transform.position = pointB;
        }

    }

    void MoveCharacter(Vector2 direction){
        if (player != null)
        {
            Vector3 playerScale = playerObj.transform.localScale;
            //Debug.Log(direction.x);
            if (direction.x < 0)
            {
                playerScale.x = 1;
                playerObj.transform.localScale = playerScale;
                ghostImage.transform.localScale = playerScale;
                //Debug.Log("player scale: " + playerScale.x);
            }
            else
            {
                playerScale.x = -1;
                playerObj.transform.localScale = playerScale;
                ghostImage.transform.localScale = playerScale;
                //Debug.Log("player scale: " + playerScale.x);
            }

            if (!IsSideScroll)
            {
                player.Translate(direction * speed * Time.deltaTime);
                ghost.Translate(direction * speed * 20 * Time.deltaTime);
            }
            else
            {
                player.Translate(new Vector2(direction.x,0) * speed * Time.deltaTime);
                ghost.Translate(direction * speed * 20 * Time.deltaTime);
            }
        }   
    }
}
