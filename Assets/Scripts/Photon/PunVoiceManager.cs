﻿using System.Collections;
using System.Collections.Generic;
using Photon.Voice.Unity;
using UnityEngine;
using UnityEngine.UI;

public class PunVoiceManager : MonoBehaviour
{
    public Recorder recorder;
    [SerializeField] private Toggle muteToggle;
    [SerializeField] private bool isMute = false;
    [SerializeField] private bool enableDebug = false;
    [SerializeField] private Text voiceDebugText;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    public void Mute()
    {
        recorder.TransmitEnabled = !muteToggle.isOn;
    }
    // Update is called once per frame
    void Update()
    {
        if (this.recorder != null && this.recorder.LevelMeter != null)
        {
            if (enableDebug)
            {
                voiceDebugText.enabled = true;
                this.voiceDebugText.text = string.Format("Amp: avg. {0:0.000000}, peak {1:0.000000}", this.recorder.LevelMeter.CurrentAvgAmp, this.recorder.LevelMeter.CurrentPeakAmp);
            }
            else
            {
                voiceDebugText.enabled = false;
            }
        }
    }
}
