﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RandomBall : MonoBehaviour
{
    // Start is called before the first frame update
    [SerializeField]private SpriteRenderer spriteRenderer;
    public Sprite[] sprites;
    void Start()
    {
        int rand = Random.Range(0, sprites.Length);
        spriteRenderer.sprite = sprites[rand];
    }
    
}
