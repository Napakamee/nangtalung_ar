﻿using System;
using Photon.Pun;
using UnityEngine;

namespace BallMinigame
{
    public class PunBall : MonoBehaviourPun, IPunInstantiateMagicCallback
    {
        public float bulletForce = 10f;
        private bool isHit = false;
        int OwnerViewID = -1;
        [SerializeField] private AudioClip LaunchingSFX;
        [SerializeField] private AudioClip HitSFX;
        [SerializeField] private AudioSource BallSFX;
        
        public void OnPhotonInstantiate(PhotonMessageInfo info)
        {
            // e.g. store this gameobject as this player's charater in Player.TagObject
            info.Sender.TagObject = this.gameObject;
            OwnerViewID = info.photonView.ViewID;

            //info.sender.TagObject = this.GameObject;
            Rigidbody2D bullet = GetComponent<Rigidbody2D>();
            // Add velocity to the bullet
            bullet.velocity = bullet.transform.up * bulletForce;

            BallSFX.clip = LaunchingSFX;
            BallSFX.Play();
            
            if (!photonView.IsMine)
                return;

            // Destroy the bullet after 10 seconds
            Destroy(this.gameObject, 3.0f);
        }
            
        private void OnTriggerEnter2D(Collider2D other)
        {
            BallSFX.clip = HitSFX;
            BallSFX.Play();
            
            if(!photonView.IsMine) return;

            if (other.CompareTag("Player"))
            {
                
                PunUserNetControl tempOther = other.gameObject.GetComponent<PunUserNetControl>();
                if (tempOther != null)
                    Debug.Log("Attack to Other ViewID : " + tempOther.photonView.ViewID);

                PlayerManager tempPlayer = other.gameObject.GetComponent<PlayerManager>();
                if(tempPlayer != null)
                {
                    
                    if (tempPlayer.playerStatus == PlayerStatus.GoodHealth 
                        || tempPlayer.playerStatus == PlayerStatus.WearMask
                        && !isHit)
                    {
                        Debug.Log("Player" + tempOther.photonView.ViewID + "is infected");
                        tempPlayer.Infected();
                        isHit = true;
                    }

                    if (tempPlayer.playerStatus == PlayerStatus.Infected && !isHit)
                    {
                        Debug.Log("Player" + tempOther.photonView.ViewID + "is death");
                        tempPlayer.Death();
                        isHit = true;
                    }
                    
                }
                Destroy(this.gameObject);
            }
            
        }
        
        private void OnDestroy()
        {
            if (!photonView.IsMine) return;
        
            PhotonNetwork.Destroy(this.gameObject);
        }
    }
}