﻿namespace BallMinigame
{
    public enum PlayerStatus
    { 
        GoodHealth = 0,
        Infected = 1,
        WearMask = 2,
        Death = 3,
        Giant = 4
    }
}