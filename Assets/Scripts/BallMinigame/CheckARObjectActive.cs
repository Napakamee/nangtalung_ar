﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CheckARObjectActive : MonoBehaviour
{
    public static bool ISObjectActive = false;

    public GameObject ARGameObject;
    

    private void Update()
    {
        if (ARGameObject.activeSelf)
        {
            ISObjectActive = true;
        }
        else
        {
            ISObjectActive = false;
        }
    }
}
