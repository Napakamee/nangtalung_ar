﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PunGameSetting
{
    public const string REMAIN_PLAYER = "Remain Player";
    public const string DEATHPLAYER = "Death Player";
    
    public const string PLAYER_READY = "IsPlayerReady";
    public const string PLAYER_LOADED_LEVEL = "PlayerLoadedLevel";

    public const string STARTGAMETIME = "GameTime";
    public const string GAMEOVER = "IsGameOver";
    public const string TIMEOUT = "IsTimeOut";
}
