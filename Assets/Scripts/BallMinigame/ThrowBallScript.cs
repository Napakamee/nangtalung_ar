﻿using Photon.Pun;
using UnityEngine;

namespace BallMinigame
{
    public class ThrowBallScript : MonoBehaviourPun
    {
        public GameObject throwPos;
        [SerializeField] private float waitTime = 0;

        [SerializeField] private float cooldown = 5;

        public GameObject ballPrefab;

        // Start is called before the first frame update
        void Start()
        {
        
        }

        private void OnTriggerEnter2D(Collider2D other)
        {
            if (!photonView.IsMine)
                return;
            if (other.gameObject.CompareTag("ThrowTriggerZone"))
            {
                if (other.transform.parent && other.GetComponent<PhotonView>().IsMine)
                {
                    Debug.Log("Hit trigger zone");
                    ThrowBall();
                }
                
            }
        }

        private void ThrowBall()
        {
            if (!photonView.IsMine)
                return;
            Debug.Log("Throw Ball!");
            if (waitTime == 0)
            {
                waitTime = cooldown;
                object[] data = { photonView.ViewID };

                PhotonNetwork.Instantiate(ballPrefab.name
                    , throwPos.transform.position + (throwPos.transform.up * 1.5f)
                    , throwPos.transform.rotation
                    , 0
                    , data);
            }
        }

        // Update is called once per frame
        void Update()
        {
            TimeUpdate();
            /*if (Input.GetKeyDown(KeyCode.Mouse0))
            {
                ThrowBall();
            }*/
        }

        private void TimeUpdate()
        {
            if (waitTime > 0)
            {
                waitTime -= Time.deltaTime;
            }
            else
            {
                waitTime = 0;
            }
        }
    }
}
