﻿using System;
using System.Collections;
using System.Collections.Generic;
using Photon.Pun;
using UnityEngine;

namespace BallMinigame
{
    public class Item : MonoBehaviourPun
    {
        public ItemType itemType;

        private void OnTriggerEnter2D(Collider2D other)
        {
            if(!photonView.IsMine) return;
            if (other.CompareTag("Player"))
            {
                Destroy(this.gameObject);
            }
        }

        private void OnDestroy()
        {
            if (!photonView.IsMine) return;
        
            PhotonNetwork.Destroy(this.gameObject);
        }
    }
}