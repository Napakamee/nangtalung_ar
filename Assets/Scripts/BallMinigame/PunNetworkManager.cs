﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using Photon.Pun;
using Photon.Pun.UtilityScripts;
using Photon.Realtime;
using SimpleJSON;
using UnityEngine.SceneManagement;
using Hashtable = ExitGames.Client.Photon.Hashtable;
using Random = UnityEngine.Random;

namespace BallMinigame
{
    public class PunNetworkManager : ConnectAndJoinRandom
    {
        public static PunNetworkManager singleton;

        [Header("Spawn Info")] [Tooltip("The prefab to use for representing the player")]
        public GameObject[] GamePlayerPrefab;
        public GameObject[] itemPrefab;
        public GameObject[] itemSpawnPoint;
        public bool isGameStart = false;
        public bool isFirstSetting = false;

        [Header("GameManager")]
        public Camera SceneCamera;

        public int Mode = 0;

        [SerializeField] private GameObject leaveCanvas;
        [SerializeField] private AudioClip LeavingSFX;
        public bool isGameOver = false;
        public bool isTimeOut = false;
        public bool isGiantWin = false;
        public int remainPlayer;
        public int deathPlayer;
        public int remainEnemy;
        public GameObject playerWinCanvas;
        public GameObject giantWinCanvas;
        public int numberOfItem = 3;
        float m_count = 0;
        public float itemDropTime = 15;
        private bool isESC = false;
        [SerializeField] private int characterIndex;

        [Header("MapLoader")]
        [SerializeField] private SpriteRenderer MapImage;
        [SerializeField] private Sprite[] MapImageList;
        /// <summary>
        /// Create delegate Method
        /// </summary>
        public delegate void PlayerSpawned();
        public static event PlayerSpawned OnPlayerSpawned;

        public delegate void FirstSetting();
        public static event FirstSetting OnFirstSetting;
        
        private void Awake()
        {
            if (singleton)
            {
                DestroyImmediate(gameObject);
                return;
            }
            singleton = this;
            GetCharacterData();
            OnPlayerSpawned += SpawnPlayer;
            OnFirstSetting += FirstRoomSetting;
            
            if (PhotonNetwork.IsConnected)
            {
                if (SceneCamera != null)
                    SceneCamera.gameObject.SetActive(false);
                if (PunUserNetControl.LocalPlayerInstance is null)
                    OnPlayerSpawned();
                if (!isGameStart)
                {
                    isGameStart = true;
                    OnFirstSetting();
                }
                Debug.Log("PhotonNetwork IsConnected");
            }
            Debug.Log("NetWorkManager Awake");
            playerWinCanvas.SetActive(false);
            giantWinCanvas.SetActive(false);
            
            GetRoomData();
        }

        private void GetRoomData()
        {
            int _map = (int) PhotonNetwork.CurrentRoom.CustomProperties[GameRoomSetting.MAP];
            MapImage.sprite = MapImageList[_map];
        }

        private void OnDestroy()
        {
            OnPlayerSpawned -= SpawnPlayer;
            OnFirstSetting -= FirstRoomSetting;
        }

        public void StartTime() {
            
            float time;
            time = (float) PhotonNetwork.Time;
            //Debug.Log("StartTime : " + time);
            Hashtable props = new Hashtable {
                {PunGameSetting.STARTGAMETIME, time}
            };
            PhotonNetwork.CurrentRoom.SetCustomProperties(props);
        }
        private void GetCharacterData()
        {
            string path = Application.streamingAssetsPath + "/JsonData/PlayerCharacterIndex.json";
            string jsonString = File.ReadAllText((path));
            JSONObject charIndex = (JSONObject) JSON.Parse(jsonString);
            characterIndex = charIndex["CharIndex"];
            Debug.Log("CharIndex: " + characterIndex);
        }

        public override void OnPlayerEnteredRoom(Player newPlayer)
        {
            base.OnPlayerEnteredRoom(newPlayer);
            
            Debug.Log("New Player. " + newPlayer.ToString());
        }

        public override void OnJoinedRoom()
        {
            base.OnJoinedRoom();
            PhotonNetwork.AutomaticallySyncScene = false;
            if(SceneCamera != null)
                SceneCamera.gameObject.SetActive(false);
            
            //gameTimer.GetStartTime(PhotonNetwork.CurrentRoom.CustomProperties);
            OnPlayerSpawned();
            Debug.Log("OnJoinedRoom");
            //PhotonNetwork.CurrentRoom.CustomProperties
            //PhotonNetwork.CurrentRoom.Players[0].CustomProperties
        }

        public override void OnEnable() {
            base.OnEnable();
            // Raise Event
            PhotonNetwork.AddCallbackTarget(this);

            // LoadBalancingClient.EventReceived
            // The second way to receive custom events is to register a method
            //PhotonNetwork.NetworkingClient.EventReceived += OnEvent;
        }

        public override void OnDisable() {
            base.OnDisable();
            // Raise Event
            PhotonNetwork.RemoveCallbackTarget(this);

            // LoadBalancingClient.EventReceived
            // The second way to receive custom events is to register a method
            //PhotonNetwork.NetworkingClient.EventReceived -= OnEvent;
        }
        public void SpawnPlayer()
        {
            if (PunUserNetControl.LocalPlayerInstance == null)
            {
                Debug.Log("We are Instantiating LocalPlayer from " + SceneManagerHelper.ActiveSceneName);
                Debug.Log(GamePlayerPrefab[characterIndex].name + " is Joined");
                //PunNetworkManager.singleton.SpawnPlayer();
                // we're in a room. spawn a character for the local player. it gets synced by using PhotonNetwork.Instantiate
                PhotonNetwork.Instantiate(GamePlayerPrefab[characterIndex].name,
                    new Vector3(0f, 0f, 0f), Quaternion.identity, 0);
                //SkinLoader playerSkin = GamePlayerPrefab.GetComponent<SkinLoader>();

                isGameStart = true;
            }
            else
            {
                Debug.Log("Ignoring scene load for " + SceneManagerHelper.ActiveSceneName);
            }
        }

        public override void OnDisconnected(DisconnectCause cause)
        {
            base.OnDisconnected(cause);
            SceneManager.LoadScene("Lobby");
            /*
            if(SceneCamera != null)
                SceneCamera.gameObject.SetActive(true);*/
        }

        private void FirstRoomSetting()
        {
            isFirstSetting = true;
            m_count = itemDropTime;
            Hashtable roomCustonProps = new Hashtable
            {
                {PunGameSetting.DEATHPLAYER, 0},
                {PunGameSetting.STARTGAMETIME, (float) PhotonNetwork.Time}
            };
            PhotonNetwork.CurrentRoom.SetCustomProperties(roomCustonProps);
            
            //OnRoomPropertiesUpdate(roomCustonProps);
        }
        
        private void ItemDrop()
        {
            int spawnIndex = Random.Range(0, itemSpawnPoint.Length);
            int itemIndex = Random.Range(0, itemPrefab.Length);
            if (GameObject.FindGameObjectsWithTag("Item").Length <
                numberOfItem)
            {
                m_count -= Time.deltaTime;

                if (m_count <= 0)
                {
                    m_count = itemDropTime;
                    PhotonNetwork.InstantiateRoomObject(itemPrefab[itemIndex].name
                        , itemSpawnPoint[spawnIndex].transform.position
                        , itemSpawnPoint[spawnIndex].transform.rotation
                        , 0);
                }
            }

        }
        
        private void Update()
        {
            // ESC Pressed
            if (Input.GetKeyDown(KeyCode.Escape) && !isESC)
            {
                isESC = true;
                leaveCanvas.SetActive(true);
                SoundManagerSingleton.Instance.PlaySFX(LeavingSFX);
            }

            else if (Input.GetKeyDown(KeyCode.Escape) && isESC)
            {
                LeaveRoom();
                SoundManagerSingleton.Instance.PlaySFX(LeavingSFX);
            }
            else if (Input.anyKeyDown && isESC)
            {
                if (!Input.GetKey(KeyCode.Escape))
                {
                    isESC = false;
                    leaveCanvas.SetActive(false);
                    SoundManagerSingleton.Instance.PlaySFX(LeavingSFX);
                }
            }
            
            if (isGameStart == true)
            {
                remainPlayer = GameObject.FindGameObjectsWithTag("Player").Length;
                remainEnemy = GameObject.FindGameObjectsWithTag("Enemy").Length;
                
                if (isFirstSetting == false)
                    OnFirstSetting();
                
                //If all player death
                if (deathPlayer != 0 && 0 >= remainPlayer && !isGameOver)
                {
                    isGameOver = true;
                    isGiantWin = true;
                    GameOver();
                    Debug.Log("GiantWin!");
                }
                else if (Mode == 2 && remainEnemy <= 0)
                {
                    isGameOver = true;
                    isGiantWin = false;
                    GameOver();
                    Debug.Log("PlayerWin!");
                }

                if (isTimeOut && !isGameOver)
                {
                    isGameOver = true;
                    isGiantWin = false;
                    GameOver();
                    Debug.Log("PlayerWin!");
                }

                if(!PhotonNetwork.IsMasterClient) return;
                ItemDrop();
            }
        }

        public void OpenConfirmLeaving()
        {
            isESC = true;
            leaveCanvas.SetActive(true);
            SoundManagerSingleton.Instance.PlaySFX(LeavingSFX);
        }

        public void LeaveRoom()
        {
            PhotonNetwork.AutomaticallySyncScene = false;
            PunUserNetControl.isPlayerJoined = false;
            PunUserNetControl.LocalPlayerInstance = null;
            Debug.Log("isPlayerJoined " + PunUserNetControl.isPlayerJoined);
            if (PhotonNetwork.NetworkClientState == Photon.Realtime.ClientState.Joined)
            {
                
                //PhotonNetwork.LeaveRoom();
                PhotonNetwork.Disconnect();
                Debug.Log("Disconnected");
            }
            
            //SceneManager.UnloadSceneAsync(SceneManager.GetActiveScene().name);
            
        }

        private void GameOver()
        {
            isGameStart = false;
            if (isGiantWin)
            {
                giantWinCanvas.SetActive(true);
            }
            else
            {
                playerWinCanvas.SetActive(true);
            }
            
        }

        public override void OnRoomPropertiesUpdate(Hashtable propertiesThatChanged)
        {
            base.OnRoomPropertiesUpdate(propertiesThatChanged);
            
            SetPlayerNumber(propertiesThatChanged);
            TimeOut(propertiesThatChanged);
        }

        private void TimeOut(Hashtable propertiesThatChanged)
        {
            object timeout;
            if (propertiesThatChanged.TryGetValue(PunGameSetting.TIMEOUT, out timeout))
            {
                isTimeOut = (bool) timeout;
            }
        }

        private void SetPlayerNumber(Hashtable propertiesThatChanged)
        {
            object death;
            if (propertiesThatChanged.TryGetValue(PunGameSetting.DEATHPLAYER, out death))
            {
                deathPlayer = (int) death;
                Debug.Log("Death player: " + deathPlayer);
            }
            
            
            //remainPlayer = (int) PhotonNetwork.CurrentRoom.CustomProperties[PunGameSetting.REMAIN_PLAYER];
        }
    }
    
}