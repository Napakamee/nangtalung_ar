﻿using System;
using System.Collections.Generic;
using System.IO;
using ExitGames.Client.Photon;
using Photon.Pun;
using Photon.Realtime;
using UnityEngine;
using UnityEngine.UI;
using Hashtable = ExitGames.Client.Photon.Hashtable;
using TMPro;
using SimpleJSON;
namespace BallMinigame
{

    public class PlayerManager : MonoBehaviourPunCallbacks, IPunInstantiateMagicCallback
    {
        public PlayerStatus playerStatus;
        public SpriteRenderer[] playerPart;
        
        public bool isHit = false;
        private Rigidbody2D rb;

        [Header("Player Component")]
        [SerializeField] private Collider2D boxCollider;
        [SerializeField] private GameObject playerPref;
        [SerializeField] private GameObject throwZone;
        [SerializeField] private GameObject imageObj;
        [SerializeField] private string nameString;
        [SerializeField] private TextMeshProUGUI nameText;
        private int characterIndex;
        private string[] nameIndex;
        [SerializeField] private AudioClip HitHurt;
        [SerializeField] private AudioClip HitDead;
        [SerializeField] private AudioClip Heal;
        [SerializeField] private AudioSource PlayerSFX;
        
        [Header("Mouth")]
        [SerializeField] private Transform mouthIK;
        [SerializeField] private float mouthPosA = 0.0f;
        [SerializeField] private float mouthPosB = -0.25f;
        [SerializeField] private float mouthMoveSpeed = 0.5f;
        [SerializeField] bool keyDown = false;
        
        
        
        //prevent player from moving outside level(border)
        [Header("Clamp Value")] 
        [SerializeField] private float minClampX = -15.7f;
        [SerializeField] private float maxClampX = 15.7f;

        [SerializeField] private float minClampY = -7f;
        [SerializeField] private float maxClampY = 7f;

        [SerializeField] private float aliveClampX = 15.7f;
        [SerializeField] private float aliveClampY = 7f;
        [SerializeField] private float deathClampX = 5f;
        [SerializeField] private float deathClampY = 3f;

        private int death = 1;
        public void OnPhotonInstantiate(PhotonMessageInfo info)
        {
            if (photonView.IsMine)
            {
                
            }
        }
        
        void Start()
        {
            minClampX = -aliveClampX;
            maxClampX = aliveClampX;
            
            minClampY = -aliveClampY;
            maxClampY = aliveClampY;
            
            rb = this.GetComponent<Rigidbody2D>();
            GetName();
            //ChangeNameProperties();
        }
/*
        private void ChangeNameProperties()
        {
            if (photonView.IsMine)
            {
                Hashtable name = new Hashtable
                {
                    {"PlayerName", nameString}
                };
                PhotonNetwork.LocalPlayer.SetCustomProperties(name);
                SetName();
            }
            
            else
            {
                OnPlayerPropertiesUpdate(photonView.Owner, photonView.Owner.CustomProperties);
            }
        }

        public override void OnPlayerPropertiesUpdate(Player targetPlayer, Hashtable changedProps)
        {
            base.OnPlayerPropertiesUpdate(targetPlayer, changedProps);
            if (targetPlayer.ActorNumber == photonView.ControllerActorNr)
            {
                nameString = (string) PhotonNetwork.LocalPlayer.CustomProperties["PlayerName"];
                SetName();
            }
        }
*/
        private void GetName()
        {
            
            string path = Application.streamingAssetsPath + "/JsonData/PlayerCharacterIndex.json";
            string jsonString = File.ReadAllText((path));
            JSONObject charIndex = (JSONObject) JSON.Parse(jsonString);
            characterIndex = charIndex["CharIndex"];
            nameString = photonView.Owner.NickName;
            
            nameIndex = new string[12]
            {
                CharacterName.NUI, CharacterName.TENG, CharacterName.YODTHONG, CharacterName.SRIKAEW, CharacterName.PHUYAIPOON,
                CharacterName.KWANMUANG, CharacterName.AITHO, CharacterName.AISAMOR, CharacterName.HERMIT,
                CharacterName.GIANT, CharacterName.SPECTATOR, nameString
            };
            SetName();
        }

        

        private void SetName()
        {
            //Debug.Log(nameIndex[12]);
            nameText.text = nameString;
        }
        private void Update()
        {
            if(!photonView.IsMine) return;
            transform.position = new Vector3(
                Mathf.Clamp(transform.position.x, minClampX, maxClampX),
                Mathf.Clamp(transform.position.y, minClampY, maxClampY),
                transform.position.z);

            if (gameObject.CompareTag("PlayerDeath"))
            {
                Image ghostImage = imageObj.GetComponent<Image>();
                ghostImage.enabled = true;
            }
            
            if (playerStatus == PlayerStatus.Death && !gameObject.CompareTag("PlayerDeath"))
            {
                Death();
            }
            if (Input.GetKeyUp(KeyCode.Space))
            {
                keyDown = false;
            }
        }

        private void FixedUpdate()
        {
            if(!photonView.IsMine) return;
            MoveMouth();
        }

        private void MoveMouth()
        {
            Vector3 position = mouthIK.transform.localPosition;
            
            if (Input.GetKey(KeyCode.Space) && position.y >= mouthPosB)
            {
                keyDown = true;
                mouthIK.Translate(new Vector3(0, -mouthMoveSpeed * Time.deltaTime,0)); 
            }
            
            if (!keyDown && position.y <= mouthPosA)
            {
                mouthIK.Translate(new Vector3(0, mouthMoveSpeed * Time.deltaTime,0));
            }
            if (!keyDown && position.y >= 0)
            {
                position.y = 0;
            }
            
        }

        private void OnTriggerEnter2D(Collider2D other)
        {
            if(!photonView.IsMine) return;
            //Collect item
            if (other.CompareTag("Item"))
            {
                Item tempItem = other.GetComponent<Item>();
                
                //Vaccine
                if (tempItem.itemType == ItemType.Vaccine
                    && playerStatus != PlayerStatus.WearMask
                    && playerStatus != PlayerStatus.Giant)
                {
                    photonView.RPC("RPCured", RpcTarget.All);
                }
                
                //Mask
                if (tempItem.itemType == ItemType.Mask 
                    && playerStatus != PlayerStatus.Infected 
                    && playerStatus != PlayerStatus.Giant)
                {
                    photonView.RPC("RPCMasked", RpcTarget.All);
                }
            }
            
            if (other.CompareTag("Player"))
            {
                PlayerManager tempPlayerStat = other.gameObject.GetComponent<PlayerManager>();
                
                //Other is infectred
                if (tempPlayerStat.playerStatus == PlayerStatus.Infected
                    && playerStatus != PlayerStatus.WearMask
                    && playerStatus != PlayerStatus.Giant)
                {
                    //Get infected
                    photonView.RPC("RPCInfected", RpcTarget.All);
                    
                }

                if (tempPlayerStat.playerStatus == PlayerStatus.Infected 
                    && playerStatus == PlayerStatus.WearMask
                    && playerStatus != PlayerStatus.Giant)
                {
                    photonView.RPC("RPCured", RpcTarget.All);
                }
            }
        }
        
        public void Infected()
        {
            if (photonView != null)
                photonView.RPC("RPCInfected", RpcTarget.All);
        }

        public void Cured()
        {
            if (photonView != null)
                photonView.RPC("RPCured", RpcTarget.All);
        }

        public void Death()
        {
            if (photonView != null)
                photonView.RPC("RPCDeath", RpcTarget.All);

            if(!photonView.IsMine) return;
            minClampX = -deathClampX;
            maxClampX = deathClampX;
            minClampY = -deathClampY;
            maxClampY = deathClampY;

            //Also do something when death
        }

        [PunRPC]
        public void RPCInfected()
        {
            Debug.Log("Infected");
            for (int i = 0; i < playerPart.Length; i++)
            {
                playerPart[i].color = Color.red;
            }
            isHit = true;
            playerStatus = PlayerStatus.Infected;
            PlayerSFX.clip = HitHurt;
            PlayerSFX.Play();
        }

        [PunRPC]
        public void RPCured()
        {
            Debug.Log("Cured");
            for (int i = 0; i < playerPart.Length; i++)
            {
                playerPart[i].color = Color.white;
            }
            playerStatus = PlayerStatus.GoodHealth;
            PlayerSFX.clip = HitHurt;
            PlayerSFX.Play();
        }

        [PunRPC]
        public void RPCDeath()
        {
            Debug.Log("Death");
            this.tag = "PlayerDeath";
            boxCollider.enabled = false;
            playerPref.SetActive(false);
            throwZone.SetActive(false);
            nameText.enabled = false;
            Hashtable props = new Hashtable
            {
                {PunGameSetting.DEATHPLAYER, 1}
            };
            PhotonNetwork.CurrentRoom.SetCustomProperties(props);
            
            playerStatus = PlayerStatus.Death;
            PlayerSFX.clip = Heal;
            PlayerSFX.Play();
        }

        [PunRPC]
        public void RPCMasked()
        {
            Debug.Log("Wear mask");
            for (int i = 0; i < playerPart.Length; i++)
            {
                playerPart[i].color = Color.cyan;
            }
            playerStatus = PlayerStatus.WearMask;
            PlayerSFX.clip = Heal;
            PlayerSFX.Play();
        }

        
    }
}
