﻿using System;
using System.Collections;
using System.Collections.Generic;
using Photon.Pun;
using UnityEngine;
using UnityEngine.SceneManagement;

public class ConfirmLeaveController : MonoBehaviour
{
    private void Update()
    {
        if (Input.anyKeyDown)
        {
            if (!Input.GetKey(KeyCode.Escape))
            {
                SceneManager.UnloadSceneAsync("ConfirmLeave");
            }
        }

        if (Input.GetKeyDown(KeyCode.Escape))
        {
            if (PhotonNetwork.NetworkClientState == Photon.Realtime.ClientState.Joined)
            {

                PhotonNetwork.Disconnect();
            }
            PunUserNetControl.isPlayerJoined = false;
            SceneManager.UnloadSceneAsync("ConfirmLeave");
            SceneManager.UnloadSceneAsync(SceneManager.GetActiveScene().name);
            //PhotonNetwork.LeaveRoom();
            SceneManager.LoadScene("Lobby");
        }
    }
}
