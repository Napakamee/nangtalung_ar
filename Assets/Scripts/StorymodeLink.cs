﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StorymodeLink : MonoBehaviour
{
    public void ExternalLink(string link)
    {
        Application.OpenURL(link);
    }
}
