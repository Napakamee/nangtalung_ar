﻿using System.Collections;
using System.Collections.Generic;
using Lean.Localization;
using Photon.Pun;
using TMPro;
using UnityEngine;

public class CharacterSelectionMode : MonoBehaviourPun
{
    private string mode;

    public GameObject hermitBtn;
    public GameObject giantBtn;
    [SerializeField]private UIController uiController;
    [SerializeField] private LeanPhrase PanelLeanPharse;
    [SerializeField] private Sprite[] FreePanel;
    [SerializeField] private Sprite[] BattlePanel;
    [SerializeField] private TextMeshProUGUI[] HeaderText;

    // Start is called before the first frame update
    void Start()
    {
        uiController = GameObject.FindWithTag("UIController").GetComponent<UIController>();
        mode = uiController._gameMode;
        Debug.Log(mode);
        HideButton();
    }

    private void HideButton()
    {
        switch (mode)
        {
            case "FREESTYLE":
                PanelLeanPharse.Entries[0].Object = FreePanel[0];
                PanelLeanPharse.Entries[1].Object = FreePanel[0];
                break;
            case "MULTIPLAYER": 
                PanelLeanPharse.Entries[0].Object = BattlePanel[0];
                PanelLeanPharse.Entries[1].Object = BattlePanel[1];
                hermitBtn.SetActive(false); 
                HeaderText[0].enabled = false;
                HeaderText[1].enabled = false;
                HeaderText[2].enabled = false;
                break;
            case "COOP":
                PanelLeanPharse.Entries[0].Object = FreePanel[0];
                PanelLeanPharse.Entries[1].Object = FreePanel[0];
                hermitBtn.SetActive(false);
                giantBtn.SetActive(false);
                HeaderText[2].enabled = false;
                break;
        }
    }
}
