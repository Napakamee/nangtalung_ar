﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GhostClamp : MonoBehaviour
{

    private RectTransform _rectTransform;
    private void Start()
    {
        _rectTransform = GetComponent<RectTransform>();
    }
    // Update is called once per frame
    void Update()
    {
        _rectTransform.localPosition = new Vector3(
            Mathf.Clamp(_rectTransform.localPosition.x, -360, 360),
            Mathf.Clamp(_rectTransform.localPosition.y, -150, 150),
            _rectTransform.localPosition.z);
    }
}
