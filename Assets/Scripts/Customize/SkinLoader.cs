﻿using System;
using System.IO;
using Photon.Pun;
using Photon.Realtime;
using UnityEngine;
using SimpleJSON;
using Hashtable = ExitGames.Client.Photon.Hashtable;
public class SkinLoader : MonoBehaviourPunCallbacks, IPunInstantiateMagicCallback
{
    
    private Player player;
    public SpriteRenderer[] spritePart;

    [Header("Sprite Options")] 
    public Sprite[] mouthOption;
    public Sprite[] hairOptions;
    public Sprite[] faceOptions;
    public Sprite[] bodyOptions;
    public Sprite[] muscleOptions;
    public Sprite[] topBodyOptions;
    public Sprite[] arm1Options;
    public Sprite[] arm2Options;
    public Sprite[] handOptions;
    public Sprite[] bottomOptions;

    public int[] index;
    private int[] ex_index;

    public void OnPhotonInstantiate(PhotonMessageInfo info)
    {
        if (photonView.IsMine)
        {
            GetSkinIndex();
        }
    }

    private void Start()
    {
        ex_index = new int[spritePart.Length];
        if (index.Length != ex_index.Length)
        {
            index = new int[spritePart.Length];
        }
        ChangeSkinProperties();
    }
    
    public void GetSkinIndex()
    {
        string path = Application.streamingAssetsPath + "/JsonData/PlayerSkinIndex.json";
        string jsonString = File.ReadAllText(path);
        JSONObject playerAppearanceJson = (JSONObject)JSON.Parse(jsonString);
        index[0] = playerAppearanceJson[SkinPartName._00PLAYER_MOUTH];
        index[1] = playerAppearanceJson[SkinPartName._01PLAYER_HAIR];
        index[2] = playerAppearanceJson[SkinPartName._02PLAYER_FACE];
        index[3] = playerAppearanceJson[SkinPartName._03PLAYER_BODY];
        index[4] = playerAppearanceJson[SkinPartName._04PLAYER_MUSCLE];
        index[5] = playerAppearanceJson[SkinPartName._05PLAYER_TOPBODY];
        index[6] = playerAppearanceJson[SkinPartName._06PLAYER_ARM1];
        index[7] = playerAppearanceJson[SkinPartName._07PLAYER_ARM2];
        index[8] = playerAppearanceJson[SkinPartName._08PLAYER_HAND];
        index[9] = playerAppearanceJson[SkinPartName._09PLAYER_BOTTOM];
    }

    private void ChangeSkinProperties()
    {
        if (photonView.IsMine)
        {
            Hashtable skins = new Hashtable
            {
                {SkinPartName._00PLAYER_MOUTH, index[0]},
                {SkinPartName._01PLAYER_HAIR, index[1]},
                {SkinPartName._02PLAYER_FACE, index[2]},
                {SkinPartName._03PLAYER_BODY, index[3]},
                {SkinPartName._04PLAYER_MUSCLE, index[4]},
                {SkinPartName._05PLAYER_TOPBODY, index[5]},
                {SkinPartName._06PLAYER_ARM1, index[6]},
                {SkinPartName._07PLAYER_ARM2, index[7]},
                {SkinPartName._08PLAYER_HAND, index[8]},
                {SkinPartName._09PLAYER_BOTTOM, index[9]}
            };
            PhotonNetwork.LocalPlayer.SetCustomProperties(skins);
            for (int i = 0; i < spritePart.Length; i++)
            {
                spritePart[i].sprite = ConvertIndexToSprite(i, index[i]);
            }
        }
        else
        {
            OnPlayerPropertiesUpdate(photonView.Owner, photonView.Owner.CustomProperties);
        }
    }
    
    public Sprite ConvertIndexToSprite(int part, int index)
    {
        switch (part)
        {
            case 0: return mouthOption[index]; break;
            case 1: return hairOptions[index]; break;
            case 2: return faceOptions[index]; break;
            case 3: return bodyOptions[index]; break;
            case 4: return muscleOptions[index]; break;
            case 5: return topBodyOptions[index]; break;
            case 6: return arm1Options[index]; break;
            case 7: return arm2Options[index]; break;
            case 8: return handOptions[index]; break;
            case 9: return bottomOptions[index]; break;
        }
        return null;
    }

    Sprite ConvertPropsToSprite(int part, Hashtable props)
    {
        switch (part)
        {
            case 0: return mouthOption[(int) props[SkinPartName._00PLAYER_MOUTH]]; break;
            case 1: return hairOptions[(int) props[SkinPartName._01PLAYER_HAIR]]; break;
            case 2: return faceOptions[(int) props[SkinPartName._02PLAYER_FACE]]; break;
            case 3: return bodyOptions[(int) props[SkinPartName._03PLAYER_BODY]]; break;
            case 4: return muscleOptions[(int) props[SkinPartName._04PLAYER_MUSCLE]]; break;
            case 5: return topBodyOptions[(int) props[SkinPartName._05PLAYER_TOPBODY]]; break;
            case 6: return arm1Options[(int) props[SkinPartName._06PLAYER_ARM1]]; break;
            case 7: return arm2Options[(int) props[SkinPartName._07PLAYER_ARM2]]; break;
            case 8: return handOptions[(int) props[SkinPartName._08PLAYER_HAND]]; break;
            case 9: return bottomOptions[(int) props[SkinPartName._09PLAYER_BOTTOM]]; break;
        }
        return null;
    }
    public override void OnPlayerPropertiesUpdate(Player targetPlayer, Hashtable changedProps)
    {
        base.OnPlayerPropertiesUpdate(targetPlayer, changedProps);
        if (targetPlayer.ActorNumber == photonView.ControllerActorNr)
        {
            for (int i = 0; i < spritePart.Length; i++)
            {
                spritePart[i].sprite = ConvertPropsToSprite(i, changedProps);
            }
        }
        return;
    }
}
