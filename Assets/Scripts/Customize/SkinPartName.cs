﻿public class SkinPartName
{
    public const string _00PLAYER_MOUTH = "PlayerMouth";
    public const string _01PLAYER_HAIR = "PlayerHair";
    public const string _02PLAYER_FACE = "PlayerFace";
    public const string _03PLAYER_BODY = "PlayerBody";
    public const string _04PLAYER_MUSCLE = "PlayerMuscle";
    public const string _05PLAYER_TOPBODY = "PlayerTopBody";
    public const string _06PLAYER_ARM1 = "PlayerArm1";
    public const string _07PLAYER_ARM2 = "PlayerArm2";
    public const string _08PLAYER_HAND = "PlayerHand";
    public const string _09PLAYER_BOTTOM = "PlayerBottom";
    
    
}
