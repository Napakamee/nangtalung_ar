﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;
using SimpleJSON;

public class CustomizeController : MonoBehaviour
{
    [Header("Sprite Parts")]
    public SpriteRenderer[] spritePart;
    //public SpriteRenderer bodyPart;

    [Header("Sprite Options")] 
    public Sprite[] mouthOption;
    public Sprite[] hairOptions;
    public Sprite[] faceOptions;
    public Sprite[] bodyOptions;
    public Sprite[] muscleOptions;
    public Sprite[] topBodyOptions;
    public Sprite[] arm1Options;
    public Sprite[] arm2Options;
    public Sprite[] handOptions;
    public Sprite[] bottomOptions;
    public AudioClip ClickSFX;
    public AudioClip TypingSFX;
    
    [Space]
    public int[] index;
    private int[] ex_index;

    //private int _partTemp;
    private void Start()
    {
        ex_index = new int[spritePart.Length];
        if (index.Length != ex_index.Length)
        {
            index = new int[spritePart.Length];
        }
        GetSkinIndex();
        ApplyPart();
    }

    public void GetSkinIndex()
    {
        string path = Application.streamingAssetsPath + "/JsonData/PlayerSkinIndex.json";
        if (File.Exists(path))
        {
            string jsonString = File.ReadAllText(path);
            JSONObject playerAppearanceJson = (JSONObject) JSON.Parse(jsonString);
            index[0] = playerAppearanceJson[SkinPartName._00PLAYER_MOUTH];
            index[1] = playerAppearanceJson[SkinPartName._01PLAYER_HAIR];
            index[2] = playerAppearanceJson[SkinPartName._02PLAYER_FACE];
            index[3] = playerAppearanceJson[SkinPartName._03PLAYER_BODY];
            index[4] = playerAppearanceJson[SkinPartName._04PLAYER_MUSCLE];
            index[5] = playerAppearanceJson[SkinPartName._05PLAYER_TOPBODY];
            index[6] = playerAppearanceJson[SkinPartName._06PLAYER_ARM1];
            index[7] = playerAppearanceJson[SkinPartName._07PLAYER_ARM2];
            index[8] = playerAppearanceJson[SkinPartName._08PLAYER_HAND];
            index[9] = playerAppearanceJson[SkinPartName._09PLAYER_BOTTOM];
        }
        
    }
    
    public void ApplyPart()
    {
        for (int i = 0; i < spritePart.Length; i++)
        {
            spritePart[i].sprite = ConvertIndexToSprite(i, index[i]);
        }
        Save();
    }

    public Sprite ConvertIndexToSprite(int part, int index)
    {
        switch (part)
        {
            case 0: return mouthOption[index]; break;
            case 1: return hairOptions[index]; break;
            case 2: return faceOptions[index]; break;
            case 3: return bodyOptions[index]; break;
            case 4: return muscleOptions[index]; break;
            case 5: return topBodyOptions[index]; break;
            case 6: return arm1Options[index]; break;
            case 7: return arm2Options[index]; break;
            case 8: return handOptions[index]; break;
            case 9: return bottomOptions[index]; break;
        }
        return null;
    }
    
    private int ConvertIndexToOption(int part)
    {
        switch (part)
        {
            case 0: return mouthOption.Length; break;
            case 1: return hairOptions.Length; break;
            case 2: return faceOptions.Length; break;
            case 3: return bodyOptions.Length; break;
            case 4: return muscleOptions.Length; break;
            case 5: return topBodyOptions.Length; break;
            case 6: return arm1Options.Length; break;
            case 7: return arm2Options.Length; break;
            case 8: return handOptions.Length; break;
            case 9: return bottomOptions.Length; break;
        }

        return 0;
    }

    #region Obsoleted

    //Obsoleted CustomizeUpdate
    /*//Only run when customize character
    private void CustomizeUpdate() 
    {
        
        for (int i = 0; i < hairOptions.Length; i++)
        {
            if (i == index[_partTemp])
            {
                //spritePart[_partTemp].sprite = hairOptions[i];
                switch (_partTemp)
                {
                    case 0: spritePart[0].sprite = hairOptions[i]; break;//Hair
                    case 1: spritePart[1].sprite = bodyOptions[i]; break;//Body
                }
            }
        }
        //Body
        /*for (int i = 0; i < bodyOptions.Length; i++)
        {
            if (i == index[1])
            {
                bodyPart.sprite = bodyOptions[i];
            }
        }
    }*/
    //Obsoleted NextPart
    /*public void OldNextPart(int part)
    {
        isEdit = true;
        index[part]++;
        
        if (index[part] >= hairOptions.Length)
        {
            index[part] = 0;
        }
        //_partTemp = part;
        ApplyPart();
    }*/


    #endregion
    
    public void NextPart(int part)
    {
        index[part]++;
        
        if (index[part] >= ConvertIndexToOption(part))
        {
            index[part] = 0;
        }
        
        ApplyPart();
        SoundManagerSingleton.Instance.PlaySFX(ClickSFX);
    }
    
    public void PreviousPart(int part)
    {
        index[part]--;
        
        if (index[part] < 0)
        {
            index[part] = ConvertIndexToOption(part) - 1;
        }
        
        ApplyPart();
        SoundManagerSingleton.Instance.PlaySFX(ClickSFX);
    }

    public void ChooseColor(int color)
    {
        index[0] = color;
        index[3] = color;
        index[4] = color;
        index[6] = color;
        index[7] = color;
        index[8] = color;
        ApplyPart();
        SoundManagerSingleton.Instance.PlaySFX(ClickSFX);
    }
    
    public void Save()
    {
        JSONObject playerAppearanceJson = new JSONObject();
        playerAppearanceJson.Add(SkinPartName._00PLAYER_MOUTH,index[0]);
        playerAppearanceJson.Add(SkinPartName._01PLAYER_HAIR,index[1]);
        playerAppearanceJson.Add(SkinPartName._02PLAYER_FACE,index[2]);
        playerAppearanceJson.Add(SkinPartName._03PLAYER_BODY,index[3]);
        playerAppearanceJson.Add(SkinPartName._04PLAYER_MUSCLE,index[4]);
        playerAppearanceJson.Add(SkinPartName._05PLAYER_TOPBODY,index[5]);
        playerAppearanceJson.Add(SkinPartName._06PLAYER_ARM1,index[6]);
        playerAppearanceJson.Add(SkinPartName._07PLAYER_ARM2,index[7]);
        playerAppearanceJson.Add(SkinPartName._08PLAYER_HAND,index[8]);
        playerAppearanceJson.Add(SkinPartName._09PLAYER_BOTTOM,index[9]);
        Debug.Log(playerAppearanceJson.ToString());
        //Save Json
        string path = Application.streamingAssetsPath + "/JsonData/";
        if (!Directory.Exists(path))
        {
            Directory.CreateDirectory(path);
        }
        File.WriteAllText(path + "PlayerSkinIndex.json", playerAppearanceJson.ToString());
    }

    public void Load()
    {
        string path = Application.streamingAssetsPath + "/JsonData/PlayerSkinIndex.json";
        string jsonString = File.ReadAllText(path);
        JSONObject playerAppearanceJson = (JSONObject)JSON.Parse(jsonString);
        spritePart[0].sprite = hairOptions[playerAppearanceJson["Hair"]]; index[0] = playerAppearanceJson["Hair"];
        spritePart[1].sprite = bodyOptions[playerAppearanceJson["Body"]]; index[1] = playerAppearanceJson["Body"];
        SoundManagerSingleton.Instance.PlaySFX(ClickSFX);
    }

    public void Typing()
    {
        SoundManagerSingleton.Instance.PlaySFX(TypingSFX);
    }
}
