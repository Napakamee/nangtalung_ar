﻿using System;
using System.Collections;
using System.Collections.Generic;
using BallMinigame;
using Photon.Pun;
using Photon.Realtime;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using Hashtable = ExitGames.Client.Photon.Hashtable;

public class PlayerListingMenu : MonoBehaviourPunCallbacks
{
    [SerializeField] private Transform _content;
    [SerializeField] private PlayerListing _playerListing;
    [SerializeField] private Image MapImage;
    [SerializeField] private Sprite[] MapImageList;
    [SerializeField] private Button[] OnlyMasterButtons;
    
    private int currentMap;
    //[SerializeField] private GameObject GiantMode;
    //[SerializeField] private RectTransform ScrollView; 

    private List<PlayerListing> _listings = new List<PlayerListing>();

    private void Start()
    {
        PhotonNetwork.AutomaticallySyncScene = true;
    }

    public void ForceReloadList()
    {
        _content.DestroyChildren();
        _listings.Clear();
        GetCurrentRoomPlayer();
    }

    public override void OnLeftRoom()
    {
        _content.DestroyChildren();
        _listings.Clear();
    }

    public override void OnJoinedRoom()
    {
        GetCurrentRoomPlayer();
        
        int _map = (int)PhotonNetwork.CurrentRoom.CustomProperties[GameRoomSetting.MAP];

        MapImage.sprite = MapImageList[_map];
        
        if(PhotonNetwork.IsMasterClient)
            return;

        foreach (var button in OnlyMasterButtons)
        {
            button.interactable = false;
        }
    }

    private void GetCurrentRoomPlayer()
    {
        if(!PhotonNetwork.InRoom) return;
        foreach (KeyValuePair<int, Player> playerInfo in PhotonNetwork.CurrentRoom.Players)
        {
            AddPlayerListing(playerInfo.Value);
        }
        
        string _gamemode = (string)PhotonNetwork.CurrentRoom.CustomProperties[GameRoomSetting.GAMEMODE];
        
        /*GiantMode.SetActive(_gamemode == "MULTIPLAYER");
        if (_gamemode == "MULTIPLAYER")
        {
            ScrollView.localPosition = new Vector3(123, 14, 0);
            ScrollView.sizeDelta = new Vector2(404,190);
        }
        else
        {
            ScrollView.localPosition = new Vector3(123, -2.5f, 0);
            ScrollView.sizeDelta = new Vector2(404,223);
        }*/
    }

    private void AddPlayerListing(Player player)
    {
        int index = _listings.FindIndex(x => x.Player == player);

        if (index != -1)
        {
            _listings[index].SetPlayerInfo(player);
        }
        else
        {
            PlayerListing listing = Instantiate(_playerListing, _content);
            if (listing != null)
            {
                listing.SetPlayerInfo(player);
                _listings.Add(listing);
            }
        }
    }

    public override void OnPlayerEnteredRoom(Player newPlayer)
    {
        AddPlayerListing(newPlayer);
    }

    public override void OnPlayerLeftRoom(Player otherPlayer)
    {
        int index = _listings.FindIndex((x => x.Player == otherPlayer));
        if (index != -1)
        {
            Destroy(_listings[index].gameObject);
            _listings.RemoveAt(index);
        }
    }

    public void changeMap(int value)
    {
        if (!PhotonNetwork.IsMasterClient)
            return;
        
        int _map = (int)PhotonNetwork.CurrentRoom.CustomProperties[GameRoomSetting.MAP];
        
        currentMap = _map+value;
        if (currentMap > MapImageList.Length-1)
        {
            currentMap = 0;
        }
        else if (currentMap < 0)
        {
            currentMap = MapImageList.Length - 1;
        }

        Hashtable roomSet = new Hashtable();
        roomSet.Add(GameRoomSetting.MAP,currentMap);
        PhotonNetwork.CurrentRoom.SetCustomProperties(roomSet);
    }

    public override void OnRoomPropertiesUpdate(Hashtable propertiesThatChanged)
    {
        int _map = (int)PhotonNetwork.CurrentRoom.CustomProperties[GameRoomSetting.MAP];
        MapImage.sprite = MapImageList[_map];
    }

    public override void OnMasterClientSwitched(Player newMasterClient)
    {
        if(!PhotonNetwork.IsMasterClient)
            return;

        foreach (var button in OnlyMasterButtons)
        {
            button.interactable = true;
        }
    }

    public void StartMatch()
    {
        
        string _gamemode = (string)PhotonNetwork.CurrentRoom.CustomProperties[GameRoomSetting.GAMEMODE];
        Debug.Log(_gamemode);
        //PhotonNetwork.IsMessageQueueRunning = false;

        switch (_gamemode)
        {
            case "FREESTYLE":
                
                SceneManager.UnloadSceneAsync(SceneManager.GetActiveScene().name);
                PhotonNetwork.LoadLevel("FreeMode");
                
                break;
            case "MULTIPLAYER":
                SceneManager.UnloadSceneAsync(SceneManager.GetActiveScene().name);
                Debug.Log(SceneManager.GetActiveScene().name);
                PhotonNetwork.LoadLevel("BallMinigame");
                    PhotonNetwork.CurrentRoom.IsVisible = false;
                    PhotonNetwork.CurrentRoom.IsOpen = false;
                
                break;
            case "COOP":
                SceneManager.UnloadSceneAsync(SceneManager.GetActiveScene().name);
                PhotonNetwork.LoadLevel("Minigame2");
                    PhotonNetwork.CurrentRoom.IsVisible = false;
                    PhotonNetwork.CurrentRoom.IsOpen = false;
                break;
        }
    }
}
