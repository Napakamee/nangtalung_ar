﻿using System;
using System.Collections;
using System.Collections.Generic;
using Photon.Pun;
using Photon.Realtime;
using UnityEngine;
using ExitGames.Client.Photon;
using Photon.Pun.Simple;
using UnityEngine.UI;
using Hashtable = ExitGames.Client.Photon.Hashtable;

public class RoomController : MonoBehaviourPunCallbacks
{
    [SerializeField] private GameMode _selectedGameMode;
    [SerializeField] private GameMode[] _availableGameModes;
    [SerializeField] private string RoomName = "";

    private const string GAME_MODE = "GAMEMODE";

    public static Action<GameMode> OnJoinRoom = delegate { };
    public static Action<bool> OnRoomStatusChange = delegate { };

    private void Awake()
    {
        UIGameMode.OnGameModeSelected += HandleGameModeSelected;

        PhotonNetwork.AutomaticallySyncScene = true;
    }
    
    private void OnDestroy()
    {
        UIGameMode.OnGameModeSelected -= HandleGameModeSelected;
    }

    public override void OnCreatedRoom()
    {
        Debug.Log("Created new room.", this);
    }

    public override void OnCreateRoomFailed(short returnCode, string message)
    {
        Debug.Log("Room is existed. Choose one to join from the list.\n" + message, this);
    }

    private void HandleGameModeSelected(GameMode gameMode)
    {
        if (!PhotonNetwork.IsConnectedAndReady) return;
        if(PhotonNetwork.InRoom) return;

        _selectedGameMode = gameMode;
        Debug.Log($"Joining new {_selectedGameMode.Name} game");
        JoinPhotonRoom();
    }

    private void JoinPhotonRoom()
    {
        Hashtable expectedCustomRoomProperties = new Hashtable()
        {
            {GAME_MODE, _selectedGameMode.Name}
        };

        PhotonNetwork.JoinRandomRoom(expectedCustomRoomProperties, 0);
    }

    private void CreatePhotonRoom()
    {
        string roomName = Guid.NewGuid().ToString();
        RoomOptions ro = GetRoomOptions();

        PhotonNetwork.JoinOrCreateRoom(roomName, ro, TypedLobby.Default);
    }
    
    private RoomOptions GetRoomOptions()
    {
        RoomOptions ro = new RoomOptions();
        ro.IsOpen = true;
        ro.IsVisible = true;
        ro.MaxPlayers = _selectedGameMode.MaxPlayers;

        string[] roomProperties = { GAME_MODE };

        Hashtable customRoomProperties = new Hashtable()
            { {GAME_MODE, _selectedGameMode.Name} };

        ro.CustomRoomPropertiesForLobby = roomProperties;
        ro.CustomRoomProperties = customRoomProperties;

        return ro;
    }
    
    private void DebugPlayerList()
    {
        string players = "";
        foreach (KeyValuePair<int, Player> player in PhotonNetwork.CurrentRoom.Players)
        {
            players += $"{player.Value.NickName}, ";
        }
        Debug.Log($"Current Room Players: {players}");
    }

    public override void OnJoinedRoom()
    {
        Debug.Log($"You have joined the Photon room {PhotonNetwork.CurrentRoom.Name}");
        DebugPlayerList();

        //_selectedGameMode = GetRoomGameMode();
        OnJoinRoom?.Invoke(_selectedGameMode);
        OnRoomStatusChange?.Invoke(PhotonNetwork.InRoom);
    }

    public override void OnLeftRoom()
    {
        Debug.Log("You have left a Photon Room");
        _selectedGameMode = null;
        //_startGame = false;
        OnRoomStatusChange?.Invoke(PhotonNetwork.InRoom);            
    }
    
    /*private GameMode GetRoomGameMode()
    {
        string gameModeName = (string)PhotonNetwork.CurrentRoom.CustomProperties["GAMEMODE"];
        GameMode gameMode = null;
        for (int i = 0; i < _availableGameModes.Length; i++)
        {
            if (string.Compare(_availableGameModes[i].Name, gameModeName) == 0)
            {
                gameMode = _availableGameModes[i];
                break;
            }
        }
        return gameMode;
    }*/
}
