﻿using System;
using System.Collections;
using System.Collections.Generic;
using ExitGames.Client.Photon;
using Photon.Pun;
using Photon.Realtime;
using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using Hashtable = ExitGames.Client.Photon.Hashtable;
using Lean.Localization;
using UnityEngine.Audio;

public class UIController : MonoBehaviourPunCallbacks
{
    public enum MenuState
    {
        Welcome = 0,
        SelectMode = 1,
        SelectCharacter = 2,
        RoomList = 3,
        Room = 4,
        HowToPlay = 5,
        Option = 6,
        QRCode = 7,
        Story = 8
    }

    [SerializeField] private GameObject[] UIInWelcome;
    [SerializeField] private GameObject[] UIInSelectMode;
    [SerializeField] private GameObject[] UIInSelectCharacter;
    [SerializeField] private GameObject[] UIInRoomList;
    [SerializeField] private GameObject[] UIInRoom;
    [SerializeField] private GameObject[] UIHowToPlay;
    [SerializeField] private GameObject[] UIInOption;
    [SerializeField] private GameObject[] UIInQRCode;
    [SerializeField] private GameObject[] UIInStory;
    [SerializeField] private Slider[] VolSliders;
    [SerializeField] private Sprite[] modeTextImages;
    [Space(5)] [SerializeField] private Button UIBackButton;
    [SerializeField] private AudioMixer _audioMixer;
    [SerializeField] private LeanPhrase StoryImage;
    [SerializeField] private Sprite[] StoryImages0; 
    [SerializeField] private Sprite[] StoryImages1; 
    [SerializeField] private Sprite[] StoryImages2; 
    [SerializeField] private Sprite[] StoryImages0EN; 
    [SerializeField] private Sprite[] StoryImages1EN; 
    [SerializeField] private Sprite[] StoryImages2EN;
    private int storyNextSprite = 0;
    private int storyMaxSprite = 0;

    [SerializeField] private TMP_Text RoomName;
    [SerializeField] private PlayerListingMenu _playerListingMenu;
    [SerializeField] private RoomListingMenu _roomListingMenu;
    [SerializeField] private GameObject UIBG;
    [Header("Localize")] [SerializeField] private LeanPhrase UIBackLean;
    [SerializeField] private LeanPhrase UImodetextLean;
    [SerializeField] private LeanPhrase UIRoomName;


    private MenuState _menuState = MenuState.Welcome;
    public string _gameMode = "";
    public int _map = 0;

    public void Start()
    {
        _audioMixer.GetFloat("MasterVol", out float masterVol);
        VolSliders[0].value = masterVol;
        _audioMixer.GetFloat("MusicVol", out float MusicVol);
        VolSliders[1].value = MusicVol;
        _audioMixer.GetFloat("SFXVol", out float SFXVol);
        VolSliders[2].value = SFXVol;
    }

    public void changeUIStateButton(int menuState)
    {
        switch ((MenuState) menuState)
        {
            case MenuState.Welcome:
                WelcomeStateUI();
                break;
            case MenuState.SelectMode:
                SelectModeStateUI();
                break;
            case MenuState.SelectCharacter:
                SelectCharacterStateUI();
                break;
            case MenuState.RoomList:
                RoomListStateUI();
                break;
            case MenuState.Room:
                RoomStateUI();
                break;
            case MenuState.HowToPlay:
                HowToPlayStateUI();
                break;
            case MenuState.Option:
                OptionStateUI();
                break;
            case MenuState.QRCode:
                QRCodeStateUI();
                break;
            case MenuState.Story:
                StoryStateUI();
                break;
        }
    }

    void WelcomeStateUI()
    {
        GameObject[][] mui = new[]
            {UIInSelectMode, UIInSelectCharacter, UIInRoomList, UIInRoom, UIHowToPlay, UIInOption, UIInQRCode, UIInStory};

        foreach (var ui in mui)
        {
            foreach (var sui in ui)
            {
                sui.SetActive(false);
            }
        }

        foreach (var ui in UIInWelcome)
        {
            ui.SetActive(true);
        }

        UIBackButton.onClick.RemoveAllListeners();
    }

    void SelectModeStateUI()
    {
        GameObject[][] mui = new[]
            {UIInWelcome, UIInSelectCharacter, UIInRoomList, UIInRoom, UIHowToPlay, UIInOption, UIInQRCode, UIInStory};

        foreach (var ui in mui)
        {
            foreach (var sui in ui)
            {
                sui.SetActive(false);
            }
        }

        foreach (var ui in UIInSelectMode)
        {
            ui.SetActive(true);
        }

        UIBackButton.onClick.RemoveAllListeners();
        UIBackButton.onClick.AddListener(WelcomeStateUI);
        UIBackLean.Entries[0].Text = "Back";
        UIBackLean.Entries[1].Text = "กลับ";
        LeanLocalization.UpdateTranslations();
        //UIBackText.text = "Back";
    }

    void SelectCharacterStateUI()
    {
        GameObject[][] mui = new[]
            {UIInWelcome, UIInSelectMode, UIInRoomList, UIInRoom, UIHowToPlay, UIInOption, UIInQRCode, UIInStory};

        foreach (var ui in mui)
        {
            foreach (var sui in ui)
            {
                sui.SetActive(false);
            }
        }

        foreach (var ui in UIInSelectCharacter)
        {
            ui.SetActive(true);
        }

        UIBackButton.onClick.RemoveAllListeners();
        openCharScene();
        UIBackButton.onClick.AddListener(closeCharScene);
        UIBackButton.onClick.AddListener(SelectModeStateUI);
        UIBackLean.Entries[0].Text = "Back";
        UIBackLean.Entries[1].Text = "กลับ";
        LeanLocalization.UpdateTranslations();
    }

    public void openCharScene()
    {
        SceneManager.LoadScene("CharacterSelection", LoadSceneMode.Additive);
    }

    public void closeCharScene()
    {
        SceneManager.UnloadSceneAsync("CharacterSelection");
    }

    void RoomListStateUI()
    {
        GameObject[][] mui = new[]
            {UIInSelectMode, UIInSelectCharacter, UIInWelcome, UIInRoom, UIHowToPlay, UIInOption, UIInQRCode, UIInStory};

        foreach (var ui in mui)
        {
            foreach (var sui in ui)
            {
                sui.SetActive(false);
            }
        }

        foreach (var ui in UIInRoomList)
        {
            ui.SetActive(true);
        }

        switch (_gameMode)
        {
            case "FREESTYLE":
                UImodetextLean.Entries[0].Object = modeTextImages[0];
                UImodetextLean.Entries[1].Object = modeTextImages[1];
                LeanLocalization.UpdateTranslations();
                break;
            case "MULTIPLAYER":
                UImodetextLean.Entries[0].Object = modeTextImages[2];
                UImodetextLean.Entries[1].Object = modeTextImages[3];
                LeanLocalization.UpdateTranslations();
                break;
            case "COOP":
                UImodetextLean.Entries[0].Object = modeTextImages[4];
                UImodetextLean.Entries[1].Object = modeTextImages[5];
                LeanLocalization.UpdateTranslations();
                break;
            default:
                Debug.LogError("_gameMode's string couldn't be read. UIModeText.text won't be changed.");
                break;
        }

        UIBackButton.onClick.RemoveAllListeners();
        UIBackButton.onClick.AddListener(SelectCharacterStateUI);
        _roomListingMenu.ForceReloadList();
        UIBackLean.Entries[0].Text = "Back";
        UIBackLean.Entries[1].Text = "กลับ";
        LeanLocalization.UpdateTranslations();
    }

    public void SetGameMode(string gamemode)
    {
        _gameMode = gamemode;
    }

    void RoomStateUI()
    {
        GameObject[][] mui = new[]
            {UIInSelectMode, UIInSelectCharacter, UIInWelcome, UIInRoomList, UIHowToPlay, UIInOption, UIInQRCode, UIInStory};

        foreach (var ui in mui)
        {
            foreach (var sui in ui)
            {
                sui.SetActive(false);
            }
        }

        foreach (var ui in UIInRoom)
        {
            ui.SetActive(true);
        }

        UIBackButton.onClick.RemoveAllListeners();
        UIBackButton.onClick.AddListener(RoomListStateUI);
        UIBackButton.onClick.AddListener(LeaveRoom);
        UIBackLean.Entries[0].Text = "Back";
        UIBackLean.Entries[1].Text = "กลับ";
        LeanLocalization.UpdateTranslations();
    }

    void HowToPlayStateUI()
    {
        GameObject[][] mui = new[]
            {UIInSelectMode, UIInSelectCharacter, UIInWelcome, UIInRoomList, UIInRoom, UIInOption, UIInQRCode, UIInStory};

        foreach (var ui in mui)
        {
            foreach (var sui in ui)
            {
                sui.SetActive(false);
            }
        }

        foreach (var ui in UIHowToPlay)
        {
            ui.SetActive(true);
        }

        UIBackButton.onClick.RemoveAllListeners();
        UIBackButton.onClick.AddListener(RoomListStateUI);
        UIBackLean.Entries[0].Text = "Back";
        UIBackLean.Entries[1].Text = "กลับ";
        LeanLocalization.UpdateTranslations();
    }

    void OptionStateUI()
    {
        GameObject[][] mui = new[]
            {UIInSelectMode, UIInSelectCharacter, UIInWelcome, UIInRoomList, UIInRoom, UIHowToPlay, UIInQRCode, UIInStory};

        foreach (var ui in mui)
        {
            foreach (var sui in ui)
            {
                sui.SetActive(false);
            }
        }

        foreach (var ui in UIInOption)
        {
            ui.SetActive(true);
        }

        UIBackButton.onClick.RemoveAllListeners();
        UIBackButton.onClick.AddListener(WelcomeStateUI);
        LeanLocalization.UpdateTranslations();
    }

    void QRCodeStateUI()
    {
        GameObject[][] mui = new[]
            {UIInSelectMode, UIInSelectCharacter, UIInWelcome, UIInRoomList, UIInRoom, UIHowToPlay, UIInOption, UIInStory};

        foreach (var ui in mui)
        {
            foreach (var sui in ui)
            {
                sui.SetActive(false);
            }
        }

        foreach (var ui in UIInQRCode)
        {
            ui.SetActive(true);
        }

        UIBackButton.onClick.RemoveAllListeners();
        UIBackButton.onClick.AddListener(WelcomeStateUI);
        LeanLocalization.UpdateTranslations();
    }
    
    void StoryStateUI()
    {
        GameObject[][] mui = new[]
            {UIInSelectMode, UIInSelectCharacter, UIInWelcome, UIInRoomList, UIInRoom, UIHowToPlay, UIInOption, UIInQRCode};

        foreach (var ui in mui)
        {
            foreach (var sui in ui)
            {
                sui.SetActive(false);
            }
        }

        foreach (var ui in UIInStory)
        {
            ui.SetActive(true);
        }

        switch (_gameMode)
        {
            case "FREESTYLE":
                storyMaxSprite = StoryImages0.Length;
                LeanLocalization.UpdateTranslations();
                if (storyMaxSprite <= 0)
                {
                    SelectCharacterStateUI();
                    storyNextSprite = 0;
                    return;
                }
                StoryImage.Entries[0].Object = StoryImages0EN[0];
                StoryImage.Entries[1].Object = StoryImages0[0];
                break;
            case "MULTIPLAYER":
                storyMaxSprite = StoryImages1.Length;
                LeanLocalization.UpdateTranslations();
                if (storyMaxSprite <= 0)
                {
                    SelectCharacterStateUI();
                    storyNextSprite = 0;
                    return;
                }
                StoryImage.Entries[0].Object = StoryImages1EN[0];
                StoryImage.Entries[1].Object = StoryImages1[0];
                break;
            case "COOP":
                storyMaxSprite = StoryImages2.Length;
                LeanLocalization.UpdateTranslations();
                if (storyMaxSprite <= 0)
                {
                    SelectCharacterStateUI();
                    storyNextSprite = 0;
                    return;
                }
                StoryImage.Entries[0].Object = StoryImages2EN[0];
                StoryImage.Entries[1].Object = StoryImages2[0];
                break;
        }

        UIBackButton.onClick.RemoveAllListeners();
        LeanLocalization.UpdateTranslations();
    }

    public void NextStoryImage()
    {
        storyNextSprite++;
        if (storyNextSprite >= storyMaxSprite)
        {
            SelectCharacterStateUI();
            storyNextSprite = 0;
        }
        else
        {
            switch (_gameMode)
            {
                case "FREESTYLE":
                    StoryImage.Entries[0].Object = StoryImages0EN[storyNextSprite];
                    StoryImage.Entries[1].Object = StoryImages0[storyNextSprite];
                    break;
                case "MULTIPLAYER":
                    StoryImage.Entries[0].Object = StoryImages1EN[storyNextSprite];
                    StoryImage.Entries[1].Object = StoryImages1[storyNextSprite];
                    break;
                case "COOP":
                    StoryImage.Entries[0].Object = StoryImages2EN[storyNextSprite];
                    StoryImage.Entries[1].Object = StoryImages2[storyNextSprite];
                    break;
            }
        }
        LeanLocalization.UpdateTranslations();
    }

    void LeaveRoom()
    {
        PhotonNetwork.LeaveRoom(true);
    }

    public void CreateNewRoom()
    {
        if (!PhotonNetwork.IsConnected) return;

        RoomOptions options = new RoomOptions();
        switch (_gameMode)
        {
            case "FREESTYLE":
                options.MaxPlayers = 20;
                break;
            case "MULTIPLAYER":
                options.MaxPlayers = 8;
                break;
            case "COOP":
                options.MaxPlayers = 6;
                break;
            default:
                Debug.LogError("_gameMode's string couldn't be read. options.MaxPlayer will be set at 8");
                options.MaxPlayers = 8;
                break;
        }

        options.IsOpen = true;
        options.IsVisible = true;

        options.CustomRoomProperties = new Hashtable();
        options.CustomRoomProperties.Add(GameRoomSetting.GAMEMODE, _gameMode);
        options.CustomRoomProperties.Add(GameRoomSetting.MAP, _map);

        options.CustomRoomPropertiesForLobby = new string[]
        {
            GameRoomSetting.GAMEMODE,
        };

        PhotonNetwork.CreateRoom(PhotonNetwork.LocalPlayer.NickName, options, TypedLobby.Default);
        changeUIStateButton(4);
    }

    public override void OnJoinedRoom()
    {
        changeUIStateButton(4);
        string tempGamemode = _gameMode;
        if (LeanLocalization.CurrentLanguage == "Thai")
        {
            switch (tempGamemode)
            {
                case "FREESTYLE":
                    tempGamemode = "โรงหนังตะลุงเสมือนจริง";
                    break;
                case "MULTIPLAYER":
                    tempGamemode = "เกมบอลระเบิด ป้องกันเชื้อโควิท-19";
                    break;
                case "COOP":
                    tempGamemode = "เกมปราบยักษ์ติดเชื้อโควิท-19";
                    break;
            }
        }

        UIRoomName.Entries[0].Text = PhotonNetwork.CurrentRoom.Name + "'s Room" + " [" + _gameMode + "]";
        UIRoomName.Entries[1].Text = "ห้องของ " + PhotonNetwork.CurrentRoom.Name + " [" + tempGamemode + "]";
        LeanLocalization.UpdateTranslations();
        //RoomName.text = PhotonNetwork.CurrentRoom.Name + "'s Room" + " [" + _gameMode + "]";
        Debug.Log(PhotonNetwork.CurrentRoom.CustomProperties[GameRoomSetting.GAMEMODE]);
    }
}