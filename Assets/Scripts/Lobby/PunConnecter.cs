﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using Photon.Pun;
using Photon.Realtime;
using SimpleJSON;
public class PunConnecter : MonoBehaviourPunCallbacks
{
    public static bool firstSetting = false;
    private string[] nameIndex;

    void Start()
    {
        print("Connecting to server.");
        PhotonNetwork.GameVersion = "0.0.1";
        PhotonNetwork.ConnectUsingSettings();
        
        SetName();
        
    }

    public void SetName()
    {
        
        if (!firstSetting)
        {
            JSONObject characterIndexJson = new JSONObject();
            characterIndexJson.Add("CustomName", "");
            string folderPath = Application.streamingAssetsPath + "/JsonData/";
            if (!Directory.Exists(folderPath))
            {
                Directory.CreateDirectory(folderPath);
                File.WriteAllText(folderPath + "PlayerCharacterIndex.json", characterIndexJson.ToString());
            }
            firstSetting = true;
        }
        
        
        string path = Application.streamingAssetsPath + "/JsonData/PlayerCharacterIndex.json";
        string jsonString = File.ReadAllText((path));
        JSONObject charIndex = (JSONObject) JSON.Parse(jsonString);
        
        int characterIndex = charIndex["CharIndex"];
        string nameString = charIndex["CustomName"];
        
        nameIndex = new string[12]
        {
            CharacterName.NUI, CharacterName.TENG, CharacterName.YODTHONG, CharacterName.SRIKAEW, CharacterName.PHUYAIPOON,
            CharacterName.KWANMUANG, CharacterName.AITHO, CharacterName.AISAMOR, CharacterName.HERMIT,
            CharacterName.GIANT, CharacterName.SPECTATOR, nameString
        };
        
        if (nameIndex[characterIndex] != "")
        {
            PhotonNetwork.LocalPlayer.NickName = nameString;
        }
        else if (nameIndex[characterIndex] == "")
        {
            PhotonNetwork.LocalPlayer.NickName = "#"+ (Random.Range(0, 10000)).ToString().PadLeft(4,'0');
        }
        //Debug.Log(jsonString.ToString());
    }
    public override void OnConnectedToMaster()
    {
        print("Connected to server. Welcome, " + PhotonNetwork.LocalPlayer.NickName);
        if(!PhotonNetwork.InLobby)
            PhotonNetwork.JoinLobby();
    }

    public override void OnDisconnected(DisconnectCause cause)
    {
        print("Disconnected from server." + cause.ToString());
    }
}
