﻿using System;
using UnityEngine;
using Photon.Pun;
using Photon.Realtime;
using Hashtable = ExitGames.Client.Photon.Hashtable;

public class PlayerSetting
{
    public const string PLAYER_COLOR = "PlayerColor";
    
    public static Color GetColor(int colorChoice)
    {
        switch (colorChoice)
        {
            case 0: return Color.red;
            case 1: return new Color(1,1,1, 0.5f);
            case 2: return Color.white;
        }
        return Color.white;
    }    
}

[RequireComponent(typeof(PhotonTransformView))]
public class PunUserNetControl : MonoBehaviourPunCallbacks , IPunInstantiateMagicCallback
{

    [Tooltip("The local player instance. Use this to know if the local player is represented in the Scene")]
    public static GameObject LocalPlayerInstance;
    public static bool isPlayerJoined = false;
    public void OnPhotonInstantiate(PhotonMessageInfo info)
    {
        Debug.Log(info.photonView.Owner.ToString());
        Debug.Log(info.photonView.ViewID.ToString());
        
        
        PlayerSpriteSorting();
        
        // #Important
        // used in PunNetworkManager.cs
        // : we keep track of the localPlayer instance to prevent instanciation when levels are synchronized
        if (photonView.IsMine)
        {
            LocalPlayerInstance = gameObject;
            isPlayerJoined = true;
            //GetComponent<MeshRenderer>().material.color = Color.blue;
        }
        else
        {
            GetComponentInChildren<Camera>().enabled = false;
            GetComponentInChildren<AudioListener>().enabled = false;
            //GetComponent<CharacterController>().enabled = false;
        }

    }
    
    private void Update()
    {
        if (!photonView.IsMine)
            return;
    }
    private void PlayerSpriteSorting()
    {
        if (!photonView.IsMine)
        {
            SpriteRenderer[] spriteRenderers = gameObject.GetComponentsInChildren<SpriteRenderer>();
            foreach (SpriteRenderer sr in spriteRenderers)
            {
                sr.sortingOrder -= 100 * photonView.ViewID;
            }
        }
    }
    /*
    public override void OnPlayerPropertiesUpdate(Player targetPlayer, Hashtable changedProps)
    {
        base.OnPlayerPropertiesUpdate(targetPlayer, changedProps);
        if (changedProps.ContainsKey(PlayerSetting.PLAYER_COLOR) &&
            targetPlayer.ActorNumber == photonView.ControllerActorNr)
        {
            object colors;
            if (changedProps.TryGetValue(PlayerSetting.PLAYER_COLOR, out colors))
            {
                SpriteRenderer[] sr = GetComponentsInChildren<SpriteRenderer>();
                foreach (var _sr in sr)
                {
                    _sr.color = PlayerSetting.GetColor((int)colors);
                }
            }
            return;
        }
    }*/
}
