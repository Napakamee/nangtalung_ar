﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class IKClamp : MonoBehaviour
{
    void Update()
    {
        transform.localPosition = new Vector3(Mathf.Clamp(transform.localPosition.x , -4, 1),
            Mathf.Clamp(transform.localPosition.y, -1, 4.5f),
            -15f);
    }
}
