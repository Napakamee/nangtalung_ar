﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using Photon.Pun;
using UnityEngine;
using SimpleJSON;
using UnityEngine.UI;

public class CharacterSelectionController : MonoBehaviourPun
{
    
    public SpriteRenderer characterDummy;
    public GameObject[] charInfo;
    public GameObject infoPanel;
    [Header("Name")]
    public Text nameTxt;

    private string[] nameIndex;
    public string customName = "";
    public GameObject customizeField;
    public Text nameInputText;
    [Header("Customize")] public GameObject tengCustomize;
    [Header("Character Options")] public Sprite[] characterOptions;

    [Space] 
    public short index = 0;
    
    public AudioClip ClickSFX;
    
    private void Start()
    {
        nameIndex = new string[12]
        {
            CharacterName.NUI, CharacterName.TENG, CharacterName.YODTHONG, CharacterName.SRIKAEW, CharacterName.PHUYAIPOON,
            CharacterName.KWANMUANG, CharacterName.AITHO, CharacterName.AISAMOR, CharacterName.HERMIT,
            CharacterName.GIANT, CharacterName.SPECTATOR, customName
        };
        LoadJson();
        //UpdateJsonIndex();
        
    }

    public void LoadJson()
    {
        string path = Application.streamingAssetsPath + "/JsonData/PlayerCharacterIndex.json";
        string jsonString = File.ReadAllText((path));
        JSONObject charIndex = (JSONObject) JSON.Parse(jsonString);
        index = (short) charIndex["CharIndex"];
        customName = charIndex["CustomName"];
        Debug.Log("CustomName : " + customName);
        ApplyPart();
        //SoundManagerSingleton.Instance.PlaySFX(ClickSFX);
    }
    private void ApplyPart()
    {
        //Show customize part
        //customizeField.SetActive(index == 11);
        tengCustomize.SetActive(index == 11);
        infoPanel.SetActive(index != 11);
        
        for (int i = 0; i < charInfo.Length; i++)
        {
            charInfo[i].SetActive(index == i);
        }
        
        characterDummy.sprite = characterOptions[index];
        
        //charDetail.sprite = characterOptions[index];
        nameTxt.text = nameIndex[index];
        
        UpdateJsonIndex();
    }

    public void ApplyName()
    {
        customName = nameInputText.text;
        PhotonNetwork.LocalPlayer.NickName = customName;
        UpdateJsonIndex();
        SoundManagerSingleton.Instance.PlaySFX(ClickSFX);
    }
    public void NextChar()
    {
        index++;
        if (index >= characterOptions.Length)
        {
            index = 0;
        }
        ApplyPart();
        SoundManagerSingleton.Instance.PlaySFX(ClickSFX);
    }

    public void PreviousChar()
    {
        index--;
        if (index < 0)
        {
            index = (short) (characterOptions.Length - 1);
        }
        ApplyPart();
        SoundManagerSingleton.Instance.PlaySFX(ClickSFX);
    }

    public void ChooseChar(int character)
    {
        index = (short) character;
        
        ApplyPart();
        SoundManagerSingleton.Instance.PlaySFX(ClickSFX);
    }

    
    public void UpdateJsonIndex()
    {
        JSONObject characterIndexJson = new JSONObject();
        characterIndexJson.Add("CharIndex", index); 
        characterIndexJson.Add("CustomName", customName);
        Debug.Log(characterIndexJson.ToString());
        
        //Save Json
        string path = Application.streamingAssetsPath + "/JsonData/";
        if (!Directory.Exists(path))
        {
            Directory.CreateDirectory(path);
        }
        File.WriteAllText(path + "PlayerCharacterIndex.json", characterIndexJson.ToString());
    }
}
