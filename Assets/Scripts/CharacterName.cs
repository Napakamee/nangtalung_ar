﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CharacterName
{
    public const string NUI = "อ้ายหนูนุ้ย";
    public const string TENG = "อ้ายเท่ง";
    public const string YODTHONG = "นายยอดทอง";
    public const string SRIKAEW = "นายสีแก้ว";
    public const string PHUYAIPOON = "ผู้ใหญ่พูน";
    public const string KWANMUANG = "อ้ายขวัญเมือง";
    public const string AITHO = "อ้ายโถ";
    public const string AISAMOR = "อ้ายสะหม้อ";
    public const string HERMIT = "ฤาษี";
    public const string GIANT = "ยักษ์";
    public const string SPECTATOR = "";

}
