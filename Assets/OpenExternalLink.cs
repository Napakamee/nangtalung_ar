﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Runtime.InteropServices;

public class OpenExternalLink : MonoBehaviour
{
    [DllImport("__Internal")]
    private static extern void OpenNewTab(string url);
    public void ExternalLink(string url)
    {
        //Application.OpenURL("https://drive.google.com/drive/folders/1o7b1WMDSnwA3IZT0ZvtOKuRXqOJ39gWM?usp=sharing");
        #if !UNITY_EDITOR && UNITY_WEBGL
             OpenNewTab(url);
        #endif
    }
    
}

